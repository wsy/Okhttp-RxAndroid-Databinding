package netUtil;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import utils.LogUtil;

/**
 * Created by Administrator on 2016/3/21.
 */
public class OkHttpHelper {
    private static OkHttpHelper okHttpHelper;
    private final OkHttpClient client;

    private OkHttpHelper() {
        client = new OkHttpClient();
    }

    public static OkHttpHelper getInstance() {
        if (null == okHttpHelper) {
            okHttpHelper = new OkHttpHelper();
        }
        return okHttpHelper;
    }

    public String get(NetInterface netInterface) {
        Request.Builder builder = new Request.Builder().url(netInterface.getURL());
        Map<String, String> headers = netInterface.getHeaders();
        if (headers != null && headers.size() != 0)
            for (String key : headers.keySet()) {
                builder.addHeader(key, headers.get(key));
            }
        Request request = builder.build();

        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public String postJson(NetJson netJson) {
        RequestBody body = RequestBody.create(JSON, netJson.getBody());

        Request.Builder builder = new Request.Builder();

        Map<String, String> headers = netJson.getHeaders();

        if (headers != null && headers.size() > 0) {
            for (String key : headers.keySet()) {
                builder.addHeader(key, headers.get(key));
            }
        }

        Request request = builder
                .url(netJson.getUrl())
                .post(body)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            } else {
                LogUtil.log(this.getClass(), "postJson net error: " + response.body().string());
                return response.message();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "postJson exception";
        }
    }

    public String post(NetInterface netInterface) {
        Request.Builder builder = new Request.Builder().url(netInterface.getURL());
        Map<String, String> headers = netInterface.getHeaders();
        if (headers != null && headers.size() != 0)
            for (String key : headers.keySet()) {
                builder.addHeader(key, headers.get(key));
            }

        // build body
        Map<String, String> params = netInterface.getParams();
        if (params != null && params.size() != 0) {
            FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
            for (String key : params.keySet()) {
                formEncodingBuilder.add(key, params.get(key));
            }
            builder.post(formEncodingBuilder.build());
        }
        Request request = builder.build();
        System.out.println(request.urlString());
        System.out.println(request.headers());
        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String put(NetInterface netInterface) {
        Request.Builder builder = new Request.Builder().url(netInterface.getURL());
        Map<String, String> headers = netInterface.getHeaders();
        if (headers != null && headers.size() != 0)
            for (String key : headers.keySet()) {
                builder.addHeader(key, headers.get(key));
            }

        // build body
//        Map<String, String> params = netInterface.getParams();
//        if (params != null && params.size() != 0) {
//            FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();
//            for (String key : params.keySet()) {
//                formEncodingBuilder.add(key, params.get(key));
//            }
//            builder.post(formEncodingBuilder.build());
//        }
        builder.put(null);
        Request request = builder.build();
        System.out.println(request.urlString());
        System.out.println(request.headers());
        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String get(String url) {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = null;
        String result = null;
        try {
            response = client.newCall(request).execute();
            result = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void get(String url, Callback callback) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        client.newCall(request).enqueue(callback);
    }


}
