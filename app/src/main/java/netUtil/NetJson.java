package netUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by wsy on 5/19/16.
 */
public interface NetJson {
    String getUrl();

    Map<String, String> getHeaders();

    String getBody();
}
