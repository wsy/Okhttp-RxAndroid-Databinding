package netUtil;

import java.util.Map;

/**
 * Created by wsy on 4/28/16.
 */
public interface NetInterface {
    String getURL();

    Map<String, String> getHeaders();

    Map<String, String> getParams();
}
