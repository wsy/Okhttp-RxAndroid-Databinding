package dbUtil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by wsy on 4/21/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;  //数据库版本号

    private static final String DATABASE_NAME = "Test";  //数据库名称

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//        String sqldept = "create table if not exists " + TableConfig.RECORD + TableConfig.RECORD_STRUCTURE;
//        LogUtil.log(TAG, sqldept);
//        db.execSQL(sqldept);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
