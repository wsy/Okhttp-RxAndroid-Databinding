package dbUtil;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.administrator.my.db.TableConfig;
import com.example.administrator.my.db.model.DeleteModel;
import com.example.administrator.my.db.model.InsertModel;
import com.example.administrator.my.db.model.UpdateModel;

import java.lang.ref.WeakReference;

import utils.LogUtil;

/**
 * Created by wsy on 4/21/16.
 */
public class UseDatabase {
    private static final String TAG = UseDatabase.class.getSimpleName();
    private WeakReference<Context> weakReference;
    DatabaseHelper dbhelper;
    public SQLiteDatabase sqlitedatabase;

    public UseDatabase(Context context) {
        super();
        weakReference = new WeakReference<Context>(context);
    }

    //打开数据库连接
    public void opendb() {
        dbhelper = new DatabaseHelper(weakReference.get());
        sqlitedatabase = dbhelper.getWritableDatabase();
        String sqldept = "create table if not exists " + TableConfig.RECORD + TableConfig.RECORD_STRUCTURE;
        LogUtil.log(TAG, sqldept);
        sqlitedatabase.execSQL(sqldept);

        String sqlHint = "create table if not exists " + TableConfig.HINT + TableConfig.HINT_STRUCTURE;
        LogUtil.log(TAG, sqlHint);
        sqlitedatabase.execSQL(sqlHint);
    }

    //关闭数据库连接
    public void closedb() {
        if (sqlitedatabase.isOpen()) {
            sqlitedatabase.close();
        }
    }

    public void insert(InsertModel im) {
        sqlitedatabase.insert(im.table(), null, im.values());
    }

    //插入表数据

    public int update(UpdateModel um) {
        opendb();
        return sqlitedatabase.update(um.table(), um.values(), um.whereClause(), um.whereArgs());
    }

    public void delete(DeleteModel dm) {
        sqlitedatabase.delete(dm.table(), dm.whereClause(), dm.whereArgs());
    }

    public Cursor rawQuery(String sql, String[] selctionArgs) {
        if (null == sqlitedatabase)
            opendb();
        return sqlitedatabase.rawQuery(sql, selctionArgs);
    }

    public void executeSql(String sql) {
        if (null == sqlitedatabase)
            opendb();
        sqlitedatabase.execSQL(sql);
    }

}
