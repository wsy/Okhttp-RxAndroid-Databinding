package myViews;

import android.app.Dialog;
import android.content.Context;

import com.example.administrator.okhttp_rxandroid_databinding.R;

import java.util.Calendar;
import java.util.Date;

import cn.aigestudio.datepicker.cons.DPMode;
import cn.aigestudio.datepicker.views.DatePicker;
import utils.DateUtil;

/**
 * Created by wsy on 5/4/16.
 */
public class DatePickerDialog extends Dialog {
    public DatePickerDialog(Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    public DatePickerDialog(Context context) {
        super(context);
        init(context);
    }

    protected DatePickerDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init(context);
    }

    private Date date;

    public DatePickerDialog setDate(Date date) {
        this.date = date;
        return this;
    }

    private void init(Context context) {
        setContentView(R.layout.dialog_datepicker);

        DatePicker datePicker = (DatePicker) findViewById(R.id.dp);
        final Calendar calendar = Calendar.getInstance();
        if (date != null) {
            calendar.setTime(date);
        }
        datePicker.setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));
        datePicker.setMode(DPMode.SINGLE);
        datePicker.setOnDatePickedListener(new DatePicker.OnDatePickedListener() {
            @Override
            public void onDatePicked(String date) {

                if (callBack != null) {
                    callBack.call(DateUtil.getDate(date));
                }
            }
        });
    }

    public interface CallBack {
        void call(Date date);
    }

    private CallBack callBack;

    public DatePickerDialog setCallBack(CallBack callBack) {
        this.callBack = callBack;
        return this;
    }
}
