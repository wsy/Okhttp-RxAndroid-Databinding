package com.example.administrator.my.hint;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ListHintItemBinding;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by wsy on 4/27/16.
 */
public class HintAdapter extends BaseAdapter {
    private WeakReference<Context> contextWeakReference;

    public HintAdapter(Context context, List<HintRecord> hintRecords) {
        contextWeakReference = new WeakReference<Context>(context);
        this.hintRecords = hintRecords;
    }

    private List<HintRecord> hintRecords;

    /**
     * @param hintRecords
     * @deprecated
     */
    public void setHintRecords(List<HintRecord> hintRecords) {
        this.hintRecords = hintRecords;
    }

    @Override
    public int getCount() {
        return null == hintRecords ? 0 : hintRecords.size();
    }

    @Override
    public HintRecord getItem(int position) {
        return null == hintRecords ? null : hintRecords.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HintHolder hintHolder;
        if (null == convertView) {
            ListHintItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(contextWeakReference.get()), R.layout.list_hint_item, parent, false);
            convertView = binding.getRoot();
            hintHolder = new HintHolder(binding);
            convertView.setTag(hintHolder);
        } else {
            hintHolder = (HintHolder) convertView.getTag();
        }
        hintHolder.setHint(getItem(position));
        return convertView;
    }

    public class HintHolder {
        private ListHintItemBinding binding;

        HintHolder(ListHintItemBinding binding) {
            this.binding = binding;
        }

        public void setHint(HintRecord hintRecord) {
            binding.setTime(hintRecord.getTimeStr());
            binding.setDate(hintRecord.getDateStr());
        }
    }
}
