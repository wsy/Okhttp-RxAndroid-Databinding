package com.example.administrator.my.net.service;

import com.example.administrator.my.db.model.DbRecord;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface UploadSleepService {
    Observable upload(DbRecord dbRecord);
}
