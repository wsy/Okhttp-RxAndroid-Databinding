package com.example.administrator.my.statistic;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;

import com.example.administrator.my.db.AsynUtil;
import com.example.administrator.my.exercise.ExerciseDay;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityStatisticBinding;

import java.util.Calendar;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import utils.DateUtil;
import utils.LogUtil;

/**
 * @deprecated
 */
public class StatisticActivity extends Activity {
    private static final String TAG = StatisticActivity.class.getSimpleName();

    private static final int DAY = R.id.tv_day;
    private static final int WEEK = R.id.tv_week;
    private static final int MONTH = R.id.tv_month;

    private static final String LEFT_DAY = "前一天";
    private static final String LEFT_WEEK = "前一周";
    private static final String LEFT_MONTH = "上一个月";
    private static final String RIGHT_DAY = "后一天";
    private static final String RIGHT_WEEK = "后一周";
    private static final String RIGHT_MONTH = "后一个月";

    private ActivityStatisticBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        calendar = Calendar.getInstance();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_statistic);
        binding.setBtLeft(LEFT_DAY);
        binding.setBtTitle(DateUtil.getDateStr(calendar.getTime()));
        binding.setBtRight(RIGHT_DAY);
        someDay();
    }

    private int tabNow;

    private Calendar calendar;

    public void tab(View view) {
        tabNow = view.getId();
        Calendar calendar = Calendar.getInstance();
        switch (tabNow) {
            case DAY:
                binding.setBtLeft(LEFT_DAY);
                binding.setBtTitle(DateUtil.getDateStr(calendar.getTime()));
                binding.setBtRight(RIGHT_DAY);
                break;
            case WEEK:
                binding.setBtLeft(LEFT_WEEK);
                binding.setBtTitle("第" + calendar.get(Calendar.WEEK_OF_YEAR) + "周");
                binding.setBtRight(RIGHT_WEEK);
                break;
            case MONTH:
                binding.setBtLeft(LEFT_MONTH);
                binding.setBtTitle((calendar.get(Calendar.MONTH) + 1) + "月");
                binding.setBtRight(RIGHT_MONTH);
                break;
            default:
                break;
        }
    }

    public void clickLeft(View view) {
        switch (tabNow) {
            case DAY:
                LogUtil.log(TAG, "before day");
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                someDay();
                break;
            case WEEK:
                break;
            case MONTH:
                break;
            default:
                break;
        }
    }

    public void click_title(View view) {
        switch (tabNow) {
            case DAY:
                new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        change the calendar
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        someDay();
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case WEEK:

                break;
            case MONTH:
                break;
            default:
                break;
        }
    }

    public void click_right(View view) {
        switch (tabNow) {
            case DAY:
                LogUtil.log(TAG, "after day");
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                someDay();
                break;
            case WEEK:
                break;
            case MONTH:
                break;
            default:
                break;
        }
    }

    /**
     * default as the calendar has set to the aim day
     */
    private void someDay() {
        //                        change the title
        binding.setBtTitle(DateUtil.getDateStr(calendar.getTime()));
//                        fresh the data
        AsynUtil.getExerciseDay(calendar.get(Calendar.YEAR), Calendar.MONTH, Calendar.DAY_OF_MONTH)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ExerciseDay>() {
                    @Override
                    public void call(ExerciseDay exerciseDay) {
                        binding.setSteps(exerciseDay.getRun_steps());
                        binding.setTime(exerciseDay.getSport_time());
                    }
                });
    }
}

