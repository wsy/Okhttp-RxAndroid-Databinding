package com.example.administrator.my.person;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.example.administrator.my.net.imp.CustodyReq;
import com.example.administrator.my.net.imp.NetUtil;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityMyCustodyBinding;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import utils.ToastUtil;

/**
 * 查看我监护的人的页面
 */
public class MyCustodyActivity extends Activity {
    private ActivityMyCustodyBinding binding;

    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_custody);
        refresh();
    }

    private void refresh() {
        NetUtil.list()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<CustodyReq>>() {
                    @Override
                    public void call(List<CustodyReq> custodyReqs) {
                        adapter = new MyAdapter(custodyReqs);
                        binding.listView.setAdapter(adapter);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        ToastUtil.toast(MyCustodyActivity.this, "请求失败");
                    }
                });
    }


    public void back(View view) {
        onBackPressed();
    }

    public void add(View view) {
        final EditText inputServer = new EditText(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("请输入用户名").setIcon(android.R.drawable.ic_dialog_info).setView(inputServer)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, int which) {
                String input = inputServer.getText().toString();
                NetUtil.request(input)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Boolean>() {
                            @Override
                            public void call(Boolean aBoolean) {
                                dialog.dismiss();
                                if (aBoolean) {
                                    ToastUtil.toast(MyCustodyActivity.this, "请求已发出");
                                } else {
                                    ToastUtil.toast(MyCustodyActivity.this, "请求失败");
                                }
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                dialog.dismiss();
                                ToastUtil.toast(MyCustodyActivity.this, "请求失败");
                            }
                        });
            }
        });
        builder.show();
    }

    class MyAdapter extends BaseAdapter {
        MyAdapter(List<CustodyReq> custodyReqs) {
            this.custodyReqs = custodyReqs;
        }

        private List<CustodyReq> custodyReqs;

        @Override
        public int getCount() {
            return custodyReqs.size();
        }

        @Override
        public CustodyReq getItem(int position) {
            return custodyReqs.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (null == convertView) {
                viewHolder = new ViewHolder();
                LayoutInflater mInflater = LayoutInflater.from(MyCustodyActivity.this);
                convertView = mInflater.inflate(R.layout.custody_item, null);
                viewHolder.nameTv = (TextView) convertView.findViewById(R.id.nameTv);
                viewHolder.timeTv = (TextView) convertView.findViewById(R.id.timeTv);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.nameTv.setText(getItem(position).getGuardianUserName());
            viewHolder.timeTv.setText(getItem(position).getDateTime());
            return convertView;
        }

        class ViewHolder {
            TextView nameTv;
            TextView timeTv;
        }
    }
}
