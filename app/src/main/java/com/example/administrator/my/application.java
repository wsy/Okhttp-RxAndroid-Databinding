package com.example.administrator.my;

import android.app.Application;
import android.content.Context;

import com.example.administrator.my.net.net.GetFallDayNet;

import dbUtil.UseDatabase;

import netUtil.OkHttpHelper;
import utils.LogUtil;

/**
 * Created by wsy on 4/21/16.
 */
public class application extends Application {
    private static final String test = "Basic aXVNVkk4eEZidnJjZHBackRRRFZoZG1jTG1JdDVEZ0lpdU1WSTh4RmJ2cmNkcFpyRFFEVmhkbWNMbUl0NURnSWl1TVZJOHhGYnZyY2RwWnJEUURWaGRtY0xtSXQ1RGdsOg==";

    private static final String TAG = application.class.getSimpleName();
    public static UseDatabase useDatabase;
    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this.getApplicationContext();
//        LogUtil.log(TAG, "application oncreate");
        useDatabase = new UseDatabase(application.this);
//        new Thread() {
//            @Override
//            public void run() {
////                LogUtil.log(TAG, OkHttpHelper.getInstance().post(new UploadFallNet("1", "wsy", "2016-12-03 10:00:00")));
//                LogUtil.log(TAG, OkHttpHelper.getInstance().get(new GetFallDayNet("2016-02-03")));
//            }
//        }.start();


    }

    @Override
    public void onTerminate() {
        context = null;
        LogUtil.log(TAG, "application onTerminate");
        super.onTerminate();
    }
}
