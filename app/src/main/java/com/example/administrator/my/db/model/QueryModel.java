package com.example.administrator.my.db.model;

/**
 * Created by wsy on 4/21/16.
 */
public interface QueryModel extends DbModel, DeleteModel {
    String[] columns();

    String groupBy();

    String having();

    String orderBy();

    String limit();
}
