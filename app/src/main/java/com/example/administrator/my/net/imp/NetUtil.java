package com.example.administrator.my.net.imp;

import android.media.session.MediaSession;

import com.example.administrator.my.db.model.DbRecord;
import com.example.administrator.my.db.model.Record;
import com.example.administrator.my.net.net.CustodyAllowNet;
import com.example.administrator.my.net.net.CustodyList;
import com.example.administrator.my.net.net.CustodyNet;
import com.example.administrator.my.net.net.CustodyRefNet;
import com.example.administrator.my.net.net.CustodyReqNet;
import com.example.administrator.my.net.net.RegisterNet;
import com.example.administrator.my.net.net.TokenNet;
import com.example.administrator.my.net.net.UploadFallNet;
import com.google.android.gms.common.api.BooleanResult;

import java.util.List;

import netUtil.OkHttpHelper;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;

/**
 * Created by wsy on 4/28/16.
 */
public class NetUtil {
    /**
     * @param dbRecord
     * @return
     * @deprecated
     */
    public static Observable<DbRecord> uploadRecord(final DbRecord dbRecord) {
        return Observable.create(new Observable.OnSubscribe<DbRecord>() {
            @Override
            public void call(Subscriber<? super DbRecord> subscriber) {
                if (dbRecord.getHasUpdate() == DbRecord.NOTUPDATE) {
                    if (dbRecord.getType().equals(Record.TYPE_PEDOMETER)) {

                    } else if (dbRecord.getType().equals(Record.TYPE_FALL)) {
                        String result = OkHttpHelper.getInstance().post(new UploadFallNet("1", "wsy", dbRecord.getStarttimeStr()));
                    }
                    // 如果成功上传,则通知下游修改数据库中字段
                    subscriber.onNext(dbRecord);
                    // 如果不需要修改数据库字段,则传空值
//                    subscriber.onNext(null);
                }
            }
        });
    }

    public static Observable<Boolean> token(final String code) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {

            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                TokenNet tokenNet = new TokenNet(code);
                boolean result = TokenNet.decodeResult(OkHttpHelper.getInstance().postJson(tokenNet));
                subscriber.onNext(result);
            }
        });
    }

    public static Observable<Boolean> register(final String username, final String password) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                subscriber.onNext(RegisterNet.decodeResult(OkHttpHelper.getInstance().postJson(new RegisterNet(username, password))));
            }
        });
    }

    public static Observable<List<CustodyReq>> reqList() {
        return Observable.create(new Observable.OnSubscribe<List<CustodyReq>>() {
            @Override
            public void call(Subscriber<? super List<CustodyReq>> subscriber) {
                subscriber.onNext(CustodyReqNet.decodeResult(OkHttpHelper.getInstance().post(new CustodyReqNet())));
            }
        });
    }

    public static Observable<Boolean> allow(final int id) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                subscriber.onNext(CustodyAllowNet.decodeResult(OkHttpHelper.getInstance().put(new CustodyAllowNet(id))));
            }
        });
    }

    public static Observable<Boolean> refuse(final int id) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                subscriber.onNext(CustodyRefNet.decodeResult(OkHttpHelper.getInstance().put(new CustodyRefNet(id))));
            }
        });
    }

    public static Observable<List<CustodyReq>> list() {
        return Observable.create(new Observable.OnSubscribe<List<CustodyReq>>() {
            @Override
            public void call(Subscriber<? super List<CustodyReq>> subscriber) {
                subscriber.onNext(CustodyList.decodeResult(OkHttpHelper.getInstance().post(new CustodyList())));
            }
        });
    }

    public static Observable<Boolean> request(final String userName) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                subscriber.onNext(CustodyNet.decodeResult(OkHttpHelper.getInstance().postJson(new CustodyNet(userName))));
            }
        });
    }
}
