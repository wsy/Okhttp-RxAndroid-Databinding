package com.example.administrator.my.db;

import android.database.Cursor;

import com.example.administrator.my.application;

import dbUtil.UseDatabase;
import utils.LogUtil;

/**
 * Created by wsy on 4/26/16.
 */
public class TestThread extends Thread {
    private static final String TAG = TestThread.class.getSimpleName();

    @Override
    public void run() {
        UseDatabase useDatabase = application.useDatabase;
        useDatabase.opendb();
//        useDatabase.insert(new InsertModel() {
//            @Override
//            public ContentValues values() {
//                Record record = new Record();
//                record.getValues().put(TableConfig.ADDRESS, "test-address");
//                record.getValues().put(TableConfig.HASUPDATE, 0);
//                return record.getValues();
//            }
//
//            @Override
//            public String table() {
//                return TableConfig.RECORD;
//            }
//        });
        Cursor cursor = useDatabase.rawQuery("select * from " + TableConfig.RECORD, null);
        while (cursor.moveToNext()) {
//根据列的索引直接读取  比如第0列的值
// id integer primary key autoincrement, startTime varchar(30), endTime varchar(30),
// statics varchar(30),raw text, comment varchar(100), address varchar(40), hasUpdate integer)
            int id = cursor.getInt(0);
            String startTime = cursor.getString(1);
            String endTime = cursor.getString(2);
            String statics = cursor.getString(3);
            String raw = cursor.getString(4);
            String comment = cursor.getString(5);
            String address = cursor.getString(6);
            int update = cursor.getInt(7);
            LogUtil.log(TAG, id + " " + startTime + " " + endTime + " " + statics + " " + raw + " " + comment + " " + address + " " + update);

//            for (String name : cursor.getColumnNames()) {
//                LogUtil.log(TAG, name);
//            }
        }
    }
}
