package com.example.administrator.my.exercise;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ListFooter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2016/3/23.
 */
public class ExerciseListView extends ListView {
    private static final String TAG = ExerciseListView.class.getSimpleName();

    public ExerciseListView(Context context) {
        super(context);
        init(context);
    }

    public ExerciseListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ExerciseListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public interface StateListener {
        void reachBottom();
    }

    private StateListener listener;
    private boolean isLoading;

    public void setStateLitener(StateListener listener) {
        this.listener = listener;
    }

    public void hideFooter() {
        Log.i(TAG, "hide footer");
        isLoading = false;
        footer.setVisibility(View.GONE);
    }

    private View footer;

    private void init(Context context) {
        // inflate footer and hide
        ListFooter binding =
                DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.list_footer_loading, this, false);
        binding.setHint(context.getString(R.string.loading));
        footer = binding.footView;
        footer.setVisibility(View.GONE);
        isLoading = false;
        this.addFooterView(binding.getRoot());

        // inform of reaching bottom
        this.setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (!isLoading && listener != null && OnScrollListener.SCROLL_STATE_IDLE == scrollState && view.getLastVisiblePosition() == view.getCount() - 1) {
                    Log.i(TAG, "reach bottom");
                    isLoading = true;
                    listener.reachBottom();
                    footer.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
    }
}
