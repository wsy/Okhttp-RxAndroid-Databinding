package com.example.administrator.my.db.model;

/**
 * Created by wsy on 4/21/16.
 */
public interface UpdateModel extends DbModel, DeleteModel, InsertModel {
}
