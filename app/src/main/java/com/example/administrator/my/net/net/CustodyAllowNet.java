package com.example.administrator.my.net.net;

import org.apache.commons.io.output.TaggedOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetInterface;
import utils.Preference;

/**
 * Created by wsy on 5/29/16.
 * 6.3. 同意请求
 */
public class CustodyAllowNet implements NetInterface {
    private static final String URL = "http://"+ NetConfig.IP+"/index.php?r=rest/v1/guardianship-request/accept&id=";

    private int id;

    public CustodyAllowNet(int id) {
        this.id = id;
    }

    @Override
    public String getURL() {
        return URL + id;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + Preference.getStr(TokenNet.ACCESS_TOKEN));
        return headers;
    }

    @Override
    public Map<String, String> getParams() {
        return null;
    }

    public static boolean decodeResult(String json) {
        try {
            JSONArray jsonArray = new JSONArray(json);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            int state = jsonObject.getInt("state");
            return 1 == state ? true : false;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }
}
