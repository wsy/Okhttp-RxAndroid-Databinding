package com.example.administrator.my.login;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.example.administrator.my.home.HomeActivity;
import com.example.administrator.my.home.TestActivity;
import com.example.administrator.my.net.imp.NetUtil;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityLoginBinding;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityNewLoginBinding;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import netUtil.NetConfig;
import netUtil.OkHttpHelper;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import utils.ToastUtil;

public class LoginActivity extends Activity {
    private ActivityNewLoginBinding binding;
    //    private ActivityLoginBinding binding;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_new_login);
        webView = binding.webView;
        openWeb();
    }

    public void register(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private static final String URL = "http://" + NetConfig.IP + "/index.php?r=oauth/authorize&response_type=code&client_id=testclient&state=xyz";

//    private static final String URL = "http://www.jianshu.com/";

    private void openWeb() {
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.loadUrl(URL);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("url is: " + url);
                if (url.equals(URL)) {
                    //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                    view.loadUrl(url);
                    return true;
                } else if (url.startsWith("https://www.getpostman.com/oauth2/callback?")) {
                    parseUrl(url);
                    return false;
                }
                return true;
            }
        });
    }

    private void parseUrl(final String url) {
        String code = null;
        try {
            java.net.URL u = new URL(url);
            String[] params = u.getQuery().split("&");
            for (String param : params) {
                String[] pairs = param.split("=");
                if (pairs[0].equals("code")) {
                    code = pairs[1];
                }
            }

            if (code != null) {
                System.out.println("code is: " + code);
                NetUtil.token(code)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Action1<Boolean>() {
                            @Override
                            public void call(Boolean aBoolean) {
                                System.out.println("get token");
                                if (aBoolean)
                                    goHome(null);
                                else {
                                    System.out.println("认证失败");
                                    ToastUtil.toast(LoginActivity.this, "登录失败!");
                                }
                            }
                        }, new Action1<Throwable>() {
                            @Override
                            public void call(Throwable throwable) {
                                throwable.printStackTrace();
                                System.out.println("网络错误");
                                ToastUtil.toast(LoginActivity.this, "登录失败!");
                            }
                        });
            } else {
                ToastUtil.toast(LoginActivity.this, "登录失败!");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            ToastUtil.toast(LoginActivity.this, "登录失败!");
        }
    }

    public void goHome(View view) {
        Intent intent = new Intent();
        intent.setClass(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
