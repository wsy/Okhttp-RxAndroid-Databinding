package com.example.administrator.my.device;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ListDeviceItemBinding;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2016/3/24.
 */
public class DeviceAdapter extends BaseAdapter {
    private List<BluetoothDevice> devices;
    private WeakReference<Context> contextWeakReference;

    public DeviceAdapter(Context context) {
        contextWeakReference = new WeakReference<>(context);
        devices = new ArrayList<>();
    }

    public void clearDevices() {
        devices.clear();
    }

    public void addDevice(BluetoothDevice device) {
        devices.add(device);
    }

    public void addDevices(List<BluetoothDevice> mdevices) {
        devices.addAll(mdevices);
    }

    public void addDevices(Set<BluetoothDevice> mdevices) {
        devices.addAll(mdevices);
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public BluetoothDevice getItem(int position) {
        return null == devices ? null : devices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DeviceHolder holder = null;
        if (null == convertView) {
            ListDeviceItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(contextWeakReference.get()), R.layout.list_device_item, parent, false);
            convertView = binding.getRoot();
            holder = new DeviceHolder(binding);
            convertView.setTag(holder);
        } else {
            holder = (DeviceHolder) convertView.getTag();
        }
        holder.setDevice(getItem(position));
        return convertView;
    }

    public class DeviceHolder {
        private ListDeviceItemBinding binding;
        private String deviceAddress;

        public String getDeviceAddress() {
            return deviceAddress;
        }

        public DeviceHolder(ListDeviceItemBinding binding) {
            this.binding = binding;
        }

        public void setDevice(BluetoothDevice device) {
            deviceAddress = device.getAddress();
            binding.setName(device.getName());
            int rid;
            switch (device.getBondState()) {
                case BluetoothDevice.BOND_BONDED:
                    rid = R.drawable.connect_ing;
                    break;
                case BluetoothDevice.BOND_NONE:
                    rid = R.drawable.connect_fail;
                    break;
                case BluetoothDevice.BOND_BONDING:
                    rid = R.drawable.connect_ing;
                    break;
                default:
                    rid = R.drawable.connect_fail;
            }
            binding.setImageRid(rid);
        }
    }

    @BindingAdapter({"bind:image"})
    public static void imageLoader(ImageView imageView, int image_rid) {
        Picasso.with(imageView.getContext()).load(image_rid).into(imageView);
    }
}
