package com.example.administrator.my.db.model;

/**
 * Created by wsy on 4/21/16.
 */
public interface DeleteModel extends DbModel {
    String whereClause();

    String[] whereArgs();
}
