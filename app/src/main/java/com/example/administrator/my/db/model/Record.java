package com.example.administrator.my.db.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.example.administrator.my.db.TableConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import utils.DateUtil;
import utils.LogUtil;

/**
 * Created by wsy on 4/21/16.
 */
public class Record {
    private static final String TAG = Record.class.getName();

    public static final String TYPE_PEDOMETER = "pedometer";
    public static final String TYPE_FALL = "fall";

    public static final String TYPE = "type";
    public static final String STARTTIME = "startTime";
    public static final String ENDTIME = "endTime";
    public static final String STATISTIC = "statistic";
    public static final String RAW = "raw";
    public static final String COMMENT = "comment";

    private String type;
    private String startTime;//S
    private String endTime;
    private String statistic;
    private String raw;
    private String comment;

    public String getType() {
        return type;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getStatistic() {
        return statistic;
    }

    public String getRaw() {
        return raw;
    }

    public String getComment() {
        return comment;
    }

    // 判断是否为记步
    public boolean isSporting() {
        if (!type.equals(TYPE_PEDOMETER))
            return false;
        return true;
    }

    private int steps;

    public int getSteps() {
        if (isSporting())
            try {
                steps = new JSONObject(statistic).getInt("steps");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        return steps;
    }

    public ContentValues getValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TYPE, type);
        contentValues.put(STARTTIME, startTime);
        contentValues.put(ENDTIME, endTime);
        contentValues.put(STATISTIC, statistic);
        contentValues.put(RAW, raw);
        contentValues.put(COMMENT, comment);
        return contentValues;
    }

    // for generate record from the db
    public Record(Cursor cursor) throws Exception {
        int typeCo = cursor.getColumnIndex(TYPE);
        int startTimeCo = cursor.getColumnIndex(STARTTIME);
        int endTimeCo = cursor.getColumnIndex(ENDTIME);
        int statisticCo = cursor.getColumnIndex(STATISTIC);
        int rawCo = cursor.getColumnIndex(RAW);
        int commentCo = cursor.getColumnIndex(COMMENT);
        if (-1 == typeCo | -1 == startTimeCo | -1 == endTimeCo | -1 == statisticCo | -1 == rawCo | -1 == commentCo)
            throw new Exception("data lack in Record");
        type = cursor.getString(typeCo);
        startTime = cursor.getString(startTimeCo);
//        LogUtil.log(TAG, "start time : " + startTime);

        endTime = cursor.getString(endTimeCo);
        statistic = cursor.getString(statisticCo);
        raw = cursor.getString(rawCo);
        comment = cursor.getString(commentCo);
    }

    // for test
    public Record() {
        type = "testType";
        endTime = startTime = "" + Calendar.getInstance().getTimeInMillis();
        statistic = "test-statistic";
        raw = "test-raw";
        comment = "test-comment";
    }

//    get data from the device
    public Record(JSONObject jsonObject) throws JSONException {
        type = jsonObject.getString(TYPE);
        startTime = jsonObject.getString(STARTTIME);
        endTime = jsonObject.getString(ENDTIME);
        statistic = jsonObject.getString(STATISTIC);
        raw = jsonObject.getString(RAW);
        comment = jsonObject.getString(COMMENT);
    }
}
