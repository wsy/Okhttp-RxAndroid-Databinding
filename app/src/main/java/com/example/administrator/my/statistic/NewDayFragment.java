package com.example.administrator.my.statistic;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.administrator.my.db.AsynUtil;
import com.example.administrator.my.exercise.ExerciseDay;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.FragmentDayBinding;

import java.util.Calendar;
import java.util.Date;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import utils.DateUtil;

public class NewDayFragment extends Fragment {
    public NewDayFragment() {
    }

    private Date date;

    public NewDayFragment setDate(Date date) {
        this.date = date;
        return this;
    }

    public Date getDate() {
        return date;
    }

    private FragmentDayBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        注意,如果使用pageradapter,这里要填true:attachToParent
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_day, container, false);
        binding.setTitleStr(DateUtil.getDateStr(date));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
//        LogUtil.log(TAG, calendar.get(Calendar.YEAR) + " " + calendar.get(Calendar.MONTH) + " " + calendar.get(Calendar.DAY_OF_MONTH));
//       GetExerciseService& GetExerciseDayService & GetDbRecordsService
        AsynUtil.getExerciseDay(calendar.get(Calendar.YEAR), Calendar.MONTH, Calendar.DAY_OF_MONTH)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ExerciseDay>() {
                    @Override
                    public void call(ExerciseDay exerciseDay) {
                        binding.setSteps(exerciseDay.getRun_steps());
                        binding.setTime(exerciseDay.getSport_time());
                    }
                });
        return binding.getRoot();
    }
}
