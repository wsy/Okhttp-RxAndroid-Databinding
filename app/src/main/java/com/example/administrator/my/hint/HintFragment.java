package com.example.administrator.my.hint;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.example.administrator.my.db.AsynUtil;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.FragmentHintBinding;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class HintFragment extends Fragment {

    public HintFragment() {
        // Required empty public constructor
    }

    private FragmentHintBinding binding;
    private HintAdapter adapter;
    private List<HintRecord> hintRecords;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hint, container, false);
        hintRecords = new ArrayList<>();
        adapter = new HintAdapter(getContext(), hintRecords);
        binding.lvHint.setAdapter(adapter);

//        item detail
        binding.lvHint.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(HintFragment.this.getContext(), HintActivity.class);
                intent.putExtra(HintActivity.INTENT_HINT, hintRecords.get(position));
                startActivity(intent);
            }
        });

        binding.btAddHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // add new hint
                Intent intent = new Intent(HintFragment.this.getContext(), HintActivity.class);
                startActivity(intent);
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        freshData();
    }

    private void freshData() {
//        GetHintsService
        AsynUtil.getHints()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<HintRecord>>() {
                    @Override
                    public void call(List<HintRecord> o) {
                        hintRecords.clear();
                        hintRecords.addAll(o);
                        adapter.notifyDataSetChanged();
                    }
                });
    }
}
