package com.example.administrator.my.net.net;

import com.example.administrator.my.application;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetJson;
import utils.LogUtil;
import utils.Preference;

/**
 * Created by wsy on 5/25/16.
 */
public class RegisterNet implements NetJson {
    private String userName;
    private String password;

    public RegisterNet(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public static final String URL = "http://" + NetConfig.IP + "/index.php?r=rest/v1/sign/register";

    @Override
    public String getUrl() {
        return URL;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Content-Type", "application/json");
        return headers;
    }

    @Override
    public String getBody() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userName", userName);
            jsonObject.put("password", password);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonObject);
            JSONObject body = new JSONObject();
            body.put("items", jsonArray);
            return body.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean decodeResult(String json) {
        try {
            JSONArray jsonArray = new JSONArray(json);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            int state = jsonObject.getInt("state");
//            JSONObject model = jsonObject.getJSONObject("model");
//            String accessToken = model.getString("accessToken");
//            Preference.putStr(application.getContext(), TokenNet.ACCESS_TOKEN, accessToken);
            return state == 1 ? true : false;
        } catch (JSONException e) {
            e.printStackTrace();
            LogUtil.log(RegisterNet.class, json);
            return false;
        }
    }
}
