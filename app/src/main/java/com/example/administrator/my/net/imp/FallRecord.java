package com.example.administrator.my.net.imp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by wsy on 5/27/16.
 */
public class FallRecord {
    private int id;
    private String userName;
    private String dateTime;

    public FallRecord(JSONObject jsonObject) {
        try {
            id = jsonObject.getInt("id");
            userName = jsonObject.getString("userName");
            dateTime = jsonObject.getString("dateTime");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getUserName() {
        return userName;
    }
}
