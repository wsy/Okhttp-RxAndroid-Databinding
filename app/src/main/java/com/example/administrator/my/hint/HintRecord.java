package com.example.administrator.my.hint;

import android.content.ContentValues;
import android.database.Cursor;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.administrator.okhttp_rxandroid_databinding.BR;
import com.example.administrator.okhttp_rxandroid_databinding.R;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by wsy on 4/27/16.
 */
public class HintRecord implements Serializable {

    public static final String ID = "id";
    public static final String HHOUR = "hhour";
    public static final String HMIN = "hmin";
    public static final String HDATE = "hdate";
    public static final String HREPEAT = "hrepeat";
    public static final String HCOMMENT = "comment";
    public static final String HREQUESTCODE = "requestCode";

    public ContentValues getValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(HHOUR, hhour);
        contentValues.put(HMIN, hmin);
        contentValues.put(HDATE, hdate);
        contentValues.put(HREPEAT, hrepeat);
        contentValues.put(HCOMMENT, hcomment);
        contentValues.put(HREQUESTCODE, requestCode);
        return contentValues;
    }

    public HintRecord(Cursor cursor) throws Exception {
        int idCo = cursor.getColumnIndex(ID);
        int hourCo = cursor.getColumnIndex(HHOUR);
        int minCo = cursor.getColumnIndex(HMIN);
        int dateCo = cursor.getColumnIndex(HDATE);
        int repeatCo = cursor.getColumnIndex(HREPEAT);
        int commentCo = cursor.getColumnIndex(HCOMMENT);
        int requestCodeCo = cursor.getColumnIndex(HREQUESTCODE);
        if (-1 == idCo || -1 == hourCo || -1 == minCo || -1 == dateCo || -1 == repeatCo || -1 == commentCo || -1 == requestCodeCo) {
            throw new Exception("");
        }
        id = cursor.getInt(idCo);
        hhour = cursor.getInt(hourCo);
        hmin = cursor.getInt(minCo);
        hdate = cursor.getInt(dateCo);
        hrepeat = cursor.getInt(repeatCo);
        hcomment = cursor.getString(commentCo);
        requestCode = cursor.getInt(requestCodeCo);
    }

    public HintRecord(HintRecord hr) {
        this.id = hr.id;
        this.hhour = hr.hhour;
        this.hmin = hr.hmin;
        this.hdate = hr.hdate;
        this.hrepeat = hr.hrepeat;
        this.hcomment = hr.hcomment;
        this.requestCode = (int) System.currentTimeMillis();
    }

    //    for new
    public HintRecord() {
        Calendar calendar = Calendar.getInstance();

        id = 0;
        hhour = calendar.get(Calendar.HOUR_OF_DAY);
        hmin = calendar.get(Calendar.MINUTE);
        hdate = calendar.get(Calendar.DAY_OF_WEEK);
        hrepeat = REPEAT;
        hcomment = "";
        requestCode = (int) System.currentTimeMillis();
    }

    // for tdate values
    public static final int H_WEEK = 0;//一周的每一天
    public static final int H_SUN = 1;//仅仅周日
    public static final int H_MON = 2;
    public static final int H_TUE = 3;
    public static final int H_WED = 4;
    public static final int H_THU = 5;
    public static final int H_FRI = 6;
    public static final int H_SAT = 7;

    // for repeat values
    public static final int REPEAT = 1;
    public static final int NOT_REPEAT = 0;

    /**
     * attention :
     * 1.当hdate为H_WEEK时,只能是重复
     * 2.当设置单次的闹钟时,默认是下一次该时间
     */

    private int id;
    private int hhour;// 小时
    private int hmin;// min
    private int hdate;//周几
    private int hrepeat;// 是否为重复
    private String hcomment;
    private int requestCode;// 设置闹钟时的请求码

    public int getId() {
        return id;
    }

    public int getRepeat() {
        return hrepeat;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public int getHdate() {
        return hdate;
    }

    public int getHhour() {
        return hhour;
    }

    public int getHmin() {
        return hmin;
    }

    public String getHcomment() {
        return hcomment;
    }

    public void setHcomment(String hcomment) {
        this.hcomment = hcomment;
    }

    public String getRepeatStr() {
        return REPEAT == hrepeat ? "是" : "否";
    }

    public void setRepeat() {
        hrepeat = REPEAT == hrepeat ? NOT_REPEAT : REPEAT;
        if (H_WEEK == hdate)
            hrepeat = REPEAT;
    }

    public void setHdate(int hdate) {
        this.hdate = hdate;
        if (H_WEEK == hdate)
            hrepeat = REPEAT;
    }

    public String getDateStr() {
        String result = "";
        if (REPEAT == hrepeat) {
            result += "每";
        }
        String suffix;
        switch (hdate) {
            case H_WEEK:
                suffix = "天";
                break;
            case H_SUN:
                suffix = "周日";
                break;
            case H_MON:
                suffix = "周一";
                break;
            case H_TUE:
                suffix = "周二";
                break;
            case H_WED:
                suffix = "周三";
                break;
            case H_THU:
                suffix = "周四";
                break;
            case H_FRI:
                suffix = "周五";
                break;
            case H_SAT:
                suffix = "周六";
                break;
            default:
                suffix = "未知的日期";
                break;
        }
        return result + suffix;
    }

    public String getTimeStr() {
        return hhour + "时 " + hmin + "分";
    }

    public void setHhour(int hhour) {
        this.hhour = hhour;
    }

    public void setHmin(int hmin) {
        this.hmin = hmin;
    }

}
