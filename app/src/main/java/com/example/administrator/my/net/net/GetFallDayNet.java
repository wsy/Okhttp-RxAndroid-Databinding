package com.example.administrator.my.net.net;

import java.util.HashMap;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetInterface;
import utils.DateUtil;
import utils.LogUtil;
import utils.StringUtil;

/**
 * Created by wsy on 4/28/16.
 */
public class GetFallDayNet implements NetInterface {
    /**
     * Basic aXVNVkk4eEZidnJjZHBackRRRFZoZG1jTG1JdDVEZ0lpdU1WSTh4RmJ2cmNkcFpyRFFEVmhkbWNMbUl0NURnSWl1TVZJOHhGYnZyY2RwWnJEUURWaGRtY0xtSXQ1RGdsOg==
     * Basic aXVNVkk4eEZidnJjZHBackRRRFZoZG1jTG1JdDVEZ0lpdU1WSTh4RmJ2cmNkcFpyRFFEVmhkbWNMbUl0NURnSWl1TVZJOHhGYnZyY2RwWnJEUURWaGRtY0xtSXQ1RGdsOg==
     */
    private static final String TAG = GetFallDayNet.class.getSimpleName();
    public static final String TOKEN = "iuMVI8xFbvrcdpZrDQDVhdmcLmIt5DgIiuMVI8xFbvrcdpZrDQDVhdmcLmIt5DgIiuMVI8xFbvrcdpZrDQDVhdmcLmIt5Dgl";
    private String dateStr;

    public GetFallDayNet(long time) {
        dateStr = DateUtil.getDateStr(time);
    }

    public GetFallDayNet(String dateStr) {
        this.dateStr = dateStr;
    }

    //    for test
    public GetFallDayNet() {
    }

    private static final String URLPREFIX = "http://" + NetConfig.IP + "/index.php?r=rest/v1/fall/view&dateTime=";

    @Override
    public String getURL() {
        return URLPREFIX + dateStr;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Basic " + StringUtil.getBase64(TOKEN + ":"));
        LogUtil.log(TAG, "Basic " + StringUtil.getBase64(TOKEN + ":"));
//        headers.put("Authorization", "Basic aXVNVkk4eEZidnJjZHBackRRRFZoZG1jTG1JdDVEZ0lpdU1WSTh4RmJ2cmNkcFpyRFFEVmhkbWNMbUl0NURnSWl1TVZJOHhGYnZyY2RwWnJEUURWaGRtY0xtSXQ1RGdsOg==");

        return headers;
    }

    @Override
    public Map<String, String> getParams() {
        return null;
    }
}
