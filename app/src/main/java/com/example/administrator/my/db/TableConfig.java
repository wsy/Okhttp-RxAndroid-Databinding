package com.example.administrator.my.db;

/**
 * Created by wsy on 4/21/16.
 */
public class TableConfig {
    //******************所有记录*******************
    public static final String ADDRESS = "address";
    public static final String HASUPDATE = "hasUpdate";

    public static final String RECORD = "record";

    //    address:设备地址; hasUpdate:是否已经上传
    public static final String RECORD_STRUCTURE = "(id integer primary key autoincrement, " +
            "type varchar(10), hasUpdate integer, startTime varchar(30), endTime varchar(30), " +
            "statistic varchar(30),raw text, comment varchar(100), address varchar(40));";

    //******************所有提示*******************
    public static final String HINT = "hint";
    // htime: 几时几分(24小时制);
    public static final String HINT_STRUCTURE = "(id integer primary key autoincrement, " +
            "comment varchar(30), hhour integer, hmin integer, hdate integer, hrepeat integer , requestCode integer)";
}
