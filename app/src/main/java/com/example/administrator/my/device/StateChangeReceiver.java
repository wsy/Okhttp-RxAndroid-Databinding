package com.example.administrator.my.device;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import utils.LogUtil;
import utils.ToastUtil;

/**
 * Created by wsy on 4/21/16.
 * <p/>
 * 用于后台监控是否有设备连接上或者原本连接的设备断开
 */
public class StateChangeReceiver extends BroadcastReceiver {
    private static final String TAG = StateChangeReceiver.class.getSimpleName();

    public StateChangeReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
//        throw new UnsupportedOperationException("Not yet implemented");
        String action = intent.getAction();
        LogUtil.log(TAG, "action is : " + action);
        BluetoothDevice device = intent
                .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        if (null == device)
            return;
        String address = device.getAddress();
        String name = device.getName();
        LogUtil.log(TAG, "address is : " + address);
        LogUtil.log(TAG, "name is : " + name);
        switch (action) {
            case BluetoothAdapter.ACTION_STATE_CHANGED:
                break;
            case BluetoothDevice.ACTION_ACL_CONNECTED://连接之后提示DeviceService停止尝试连接设备
//                Intent stopIntent = new Intent(context, DeviceService.class);
//                stopIntent.putExtra(DeviceService.MESSAGE, DeviceService.STOP_FOUND);
//                context.startService(stopIntent);
                break;
            case BluetoothDevice.ACTION_ACL_DISCONNECTED://当发现原本连接的设备断开后,再重新开始不断尝试连接设备
//                Intent startIntent = new Intent(context, DeviceService.class);
//                startIntent.putExtra(DeviceService.MESSAGE, DeviceService.START_FOUND);
//                context.startService(intent);
                break;
            case BluetoothDevice.ACTION_FOUND:
//                if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
//                    Intent serviceIntent = new Intent(context, DeviceService.class);
//                    serviceIntent.putExtra("address", address);
//                    context.startService(serviceIntent);
//                }
                break;
            default:
                break;
        }
    }
}
