package com.example.administrator.my.person;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.example.administrator.my.db.AsynUtil;
import com.example.administrator.my.db.model.DbRecord;
import com.example.administrator.my.device.DeviceActivity;
import com.example.administrator.my.device.DeviceConfig;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityPersonBinding;

import java.util.List;

import com.example.administrator.my.net.imp.NetUtil;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import utils.LogUtil;
import utils.Preference;

/**
 * Created by wsy on 4/28/16.
 */
public class PersonActivity extends Activity {
    private static final String TAG = PersonActivity.class.getSimpleName();
    private ActivityPersonBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.log(TAG, "onCreate");
        binding = DataBindingUtil.setContentView(this, R.layout.activity_person);
        binding.setNickName("wsy");
        binding.setAge("23");
        binding.setSex("男");
        binding.setAddress(Preference.getStr(this, DeviceConfig.DEVICE_ADDRESS, ""));

//        ViewFlipper viewFlipper=null;
//        viewFlipper.add
    }

    //    查看我的监护人
    public void custody(View view) {
        Intent intent = new Intent(this, CustodyActivity.class);
        startActivity(intent);
    }

    //    查看我监护的人
    public void myCustody(View view) {
        Intent intent = new Intent(this, MyCustodyActivity.class);
        startActivity(intent);
    }

    public void back(View view) {
        onBackPressed();
    }

    public void searchDevice(View view) {
        Intent intent = new Intent(this, DeviceActivity.class);
        startActivity(intent);
    }

    public void update(View view) {
//        GetAllDbRecords
        AsynUtil.getAllDbRecords()
                .flatMap(new Func1<List<DbRecord>, Observable<DbRecord>>() {
                    @Override
                    public Observable<DbRecord> call(List<DbRecord> dbRecords) {
//                        转为事件流
                        return Observable.from(dbRecords);
                    }
                })
//                过滤掉已经上传过的记录
                .filter(new Func1<DbRecord, Boolean>() {
                    @Override
                    public Boolean call(DbRecord dbRecord) {
                        return dbRecord.getHasUpdate() == DbRecord.NOTUPDATE;
                    }
                })
//                上传记录
                .flatMap(new Func1<DbRecord, Observable<DbRecord>>() {
                    @Override
                    public Observable<DbRecord> call(DbRecord dbRecord) {
//                        UploadHintService
//                        UploadExerciseService
//                        UploadSleepService
                        return NetUtil.uploadRecord(dbRecord);
                    }
                })
//                过滤掉上传失败的记录
                .filter(new Func1<DbRecord, Boolean>() {
                    @Override
                    public Boolean call(DbRecord dbRecord) {
                        return dbRecord != null;
                    }
                })
//                对于上传成功的记录,修改数据库中的字段
                .flatMap(new Func1<DbRecord, Observable<Boolean>>() {
                    @Override
                    public Observable<Boolean> call(DbRecord dbRecord) {
                        // 修改数据库中对应字段的hasupdate值
//                        UpdateRecordService
                        return AsynUtil.updateRecord(dbRecord);
                    }
                }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        LogUtil.log(TAG, "又上传一条");
                    }
                });
    }
}
