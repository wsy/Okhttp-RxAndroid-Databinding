package com.example.administrator.my.net.service;

import com.example.administrator.my.hint.HintRecord;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface UploadHintService {
    Observable upload(HintRecord hintRecord);
}
