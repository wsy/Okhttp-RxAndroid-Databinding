package com.example.administrator.my.exercise;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.administrator.my.db.model.DbRecord;
import com.example.administrator.okhttp_rxandroid_databinding.BR;

/**
 * Created by Administrator on 2016/3/23.
 * <p/>
 * exerciseInfo is the min unit to show the exercise information
 */
public class ExerciseInfo extends BaseObservable {
    public ExerciseInfo() {
    }

    private String startTimeStr;
    private String endTimeStr;
    private String isSporting;
    private String step;

    public ExerciseInfo(DbRecord dbRecord) {
        startTimeStr = dbRecord.getStarttimeStr();
        endTimeStr = dbRecord.getEndTimeStr();
        isSporting = dbRecord.isSporting() ? "运动" : "非运动";
        step = "" + dbRecord.steps();
    }

    @Bindable
    public String getStartTimeStr() {
        return startTimeStr;
    }

    @Bindable
    public String getEndTimeStr() {
        return endTimeStr;
    }

    @Bindable
    public String getIsSporting() {
        return isSporting;
    }

    @Bindable
    public String getStep() {
        return step;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
        notifyPropertyChanged(BR.startTimeStr);
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
        notifyPropertyChanged(BR.endTimeStr);
    }

    public void setIsSporting(String isSporting) {
        this.isSporting = isSporting;
        notifyPropertyChanged(BR.isSporting);
    }

    public void setStep(String step) {
        this.step = step;
        notifyPropertyChanged(BR.step);
    }
}
