package com.example.administrator.my.exercise;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ListExerciseItemBinding;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Administrator on 2016/3/23.
 */
public class ExerciseAdapter extends BaseAdapter {
    private List<ExerciseInfo> exerciseInfos;
    private WeakReference<Context> contextWeakReference;

    public ExerciseAdapter(List<ExerciseInfo> exerciseInfos, Context context) {
        this.exerciseInfos = exerciseInfos;
        contextWeakReference = new WeakReference<>(context);
    }

    @Override
    public int getCount() {
        return null == exerciseInfos ? 0 : exerciseInfos.size();
    }

    @Override
    public ExerciseInfo getItem(int position) {
        return null == exerciseInfos ? null : exerciseInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ExerciseHolder holder = null;
        if (null == convertView) {
            ListExerciseItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(contextWeakReference.get()), R.layout.list_exercise_item, parent, false);
            holder = new ExerciseHolder(binding);
            convertView = binding.getRoot();
            convertView.setTag(holder);
        } else {
            holder = (ExerciseHolder) convertView.getTag();
        }

        holder.update(getItem(position));
        return convertView;
    }

    public class ExerciseHolder {
        private ListExerciseItemBinding binding = null;

        public ExerciseHolder(ListExerciseItemBinding binding) {
            this.binding = binding;
        }

        public void update(ExerciseInfo info) {
            binding.setExerciseinfo(info);
        }

        public View getView() {
            return binding.getRoot();
        }
    }
}
