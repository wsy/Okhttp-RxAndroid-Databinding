package com.example.administrator.my.net.net;

import com.example.administrator.my.application;
import com.example.administrator.my.net.imp.PedometerRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetInterface;
import utils.Preference;

/**
 * get
 * Created by wsy on 5/27/16.
 */
public class PedometerGetNet implements NetInterface {
    private static final String URL = "http://"+ NetConfig.IP+"/index.php?r=rest/v1/step/view&dateTime=";

    private String time;

    public PedometerGetNet(String time) {
        this.time = time;
    }

    @Override
    public String getURL() {
        return URL + time;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + Preference.getStr(application.getContext(), TokenNet.ACCESS_TOKEN, ""));
        return headers;
    }

    @Override
    public Map<String, String> getParams() {
        return null;
    }

    public List<PedometerRecord> decodeResult(String json) {
        List<PedometerRecord> pedometerRecords = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            for (int i = 0; i < jsonArray.length(); i++) {
                pedometerRecords.add(new PedometerRecord(jsonArray.getJSONObject(i)));
            }
            return pedometerRecords;
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            return null;
        }
    }
}
