package com.example.administrator.my.device;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityDeviceBinding;

import bluetoothUtil.BluetoothUtil;
import utils.LogUtil;
import utils.Preference;
import utils.ToastUtil;

/**
 * Created by wsy on 4/21/16.
 * <p/>
 * 用于在展示设备连接等内容
 */
public class DeviceActivity extends Activity {
    private static final String TAG = DeviceActivity.class.getSimpleName();
    private DeviceAdapter adapter;
    private BluetoothAdapter mBluetoothAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityDeviceBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_device);
        adapter = new DeviceAdapter(this);
        binding.deviceList.setAdapter(adapter);
        binding.deviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "click " + position);
                BluetoothDevice device = adapter.getItem(position);
                final String address = device.getAddress();
                String psw = "1234";
                mBluetoothAdapter.cancelDiscovery();

                AlertDialog.Builder builder = new AlertDialog.Builder(DeviceActivity.this);
                builder.setTitle("设备号:");
                builder.setMessage(address);
//                这里的绑定不是bond,是设备和"帐号"的绑定即应用的绑定关系
                builder.setPositiveButton("绑定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BluetoothDevice device = BluetoothUtil.pair(address, "1234");
                        if (device != null) {
                            Preference.putStr(DeviceActivity.this, DeviceConfig.DEVICE_ADDRESS, address);
                            LogUtil.log(TAG, address);
                            LogUtil.log(TAG, "address : " + Preference.getStr(DeviceActivity.this, DeviceConfig.DEVICE_ADDRESS, ""));
//                            Intent intent = new Intent(DeviceActivity.this, DeviceService.class);
//                            intent.putExtra(DeviceService.MESSAGE, DeviceService.START_FOUND);
//                            startService(intent);
                            new ReceiveThread(address).start();
                        } else
                            ToastUtil.toast(DeviceActivity.this, "绑定失败");
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();
            }
        });

        binding.deviceList.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                DeviceAdapter.DeviceHolder deviceHolder = (DeviceAdapter.DeviceHolder) v.getTag();
                final String address = deviceHolder.getDeviceAddress();
                AlertDialog.Builder builder = new AlertDialog.Builder(DeviceActivity.this);
                builder.setTitle("设备号:");
                builder.setMessage(deviceHolder.getDeviceAddress());
//                这里的绑定不是bond,是设备和"帐号"的绑定即应用的绑定关系
                builder.setPositiveButton("绑定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BluetoothDevice device = BluetoothUtil.pair(address, "1234");
                        if (device != null)
                            Preference.putStr(DeviceActivity.this, DeviceConfig.DEVICE_ADDRESS, address);
                        else
                            ToastUtil.toast(DeviceActivity.this, "绑定失败");
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();
                return false;
            }
        });

        // show bonded devices
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        showBondedDevices();

        // 注册用以接收到已搜索到的蓝牙设备的receiver
        IntentFilter mFilter = new IntentFilter();
        mFilter.addAction(BluetoothDevice.ACTION_FOUND);
        // 注册搜索完时的receiver
        mFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mReceiver, mFilter);
    }

    private void showBondedDevices() {
        adapter.addDevices(mBluetoothAdapter.getBondedDevices());
        adapter.notifyDataSetChanged();
    }

    public void click_search(View view) {
        openBlueTooth();
        setProgressBarIndeterminateVisibility(true);
        setTitle("正在扫描....");
        // 如果正在搜索，就先取消搜索
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
        // 开始搜索蓝牙设备,搜索到的蓝牙设备通过广播返回
        adapter.clearDevices();
        showBondedDevices();
        mBluetoothAdapter.startDiscovery();
    }

    private void openBlueTooth() {
        // jump to the settings & open ths bluetooth
        if (!mBluetoothAdapter.isEnabled()) {
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            // 设置蓝牙可见性，最多300秒
            intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //解除注册
        unregisterReceiver(mReceiver);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub

            String action = intent.getAction();
            // 获得已经搜索到的蓝牙设备
            if (action.equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // 搜索到的不是已经绑定的蓝牙设备
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    adapter.addDevice(device);
                    adapter.notifyDataSetChanged();
                }
                // 搜索完成
            } else if (action
                    .equals(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)) {
                setProgressBarIndeterminateVisibility(false);
                setTitle("搜索蓝牙设备");
            }
        }
    };
}
