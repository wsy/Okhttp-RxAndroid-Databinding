package com.example.administrator.my.statistic;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityViewPagerBinding;

import java.util.Calendar;
import java.util.Date;

import myViews.DatePickerDialog;
import utils.LogUtil;
import utils.ToastUtil;

public class ViewPagerActivity extends FragmentActivity {
    private static final String TAG = ViewPagerActivity.class.getSimpleName();
    private ActivityViewPagerBinding binding;
    private MyFragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_pager);
//        adapter = new MyPagerAdapter(this, binding.vpContent);
//        TestPagerAdapter adapter = new TestPagerAdapter(this, binding.vpContent);
        adapter = new MyFragmentPagerAdapter(getSupportFragmentManager(), binding.vpContent);
        dayTab();
    }

    private void dayTab() {
        binding.vpContent.setAdapter(adapter);
        binding.vpContent.addOnPageChangeListener(adapter);
        binding.vpContent.setCurrentItem(MyFragmentPagerAdapter.OFFSET);
    }

    public void tab(View view) {
        binding.btDay.setBackgroundResource(R.drawable.round_red);
        binding.btWeek.setBackgroundResource(R.drawable.round_red);
        binding.btMonth.setBackgroundResource(R.drawable.round_red);
        switch (view.getId()) {
            case R.id.bt_day:
                binding.btDay.setBackgroundResource(R.drawable.round_blue);
                dayTab();
                break;
            case R.id.bt_week:
                binding.btWeek.setBackgroundResource(R.drawable.round_blue);
                binding.vpContent.setAdapter(weekAdapter);
                break;
            case R.id.bt_month:
                binding.btMonth.setBackgroundResource(R.drawable.round_blue);
                binding.vpContent.setAdapter(monthAdapter);
                break;
            default:
                ToastUtil.toast(this, "error");
                break;
        }
    }

    public void chooseDate(View view) {
        final DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.ShareDialog);
        datePickerDialog.setDate(adapter.getNowDate()).setCallBack(new DatePickerDialog.CallBack() {
            @Override
            public void call(Date date) {
                LogUtil.log(TAG, "choose date is : " + date);
                Calendar calendar = Calendar.getInstance();
                if (date.getTime() > calendar.getTime().getTime()) {
                    ToastUtil.toast(ViewPagerActivity.this, "无效日期");
                    return;
                }
                adapter.resetDate(date);
                datePickerDialog.dismiss();
            }
        }).show();
    }

    private FragmentStatePagerAdapter weekAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
        @Override
        public Fragment getItem(int position) {
            return new WeekFragment();
        }

        @Override
        public int getCount() {
            return 1;
        }
    };

    private FragmentStatePagerAdapter monthAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
        @Override
        public Fragment getItem(int position) {
            return new MonthFragment();
        }

        @Override
        public int getCount() {
            return 1;
        }
    };
}
