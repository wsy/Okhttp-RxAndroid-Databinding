package com.example.administrator.my.db.service;

import com.example.administrator.my.db.model.Record;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface SaveRecordService {
    Observable save(Record record);
}
