package com.example.administrator.my.person;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.administrator.my.net.imp.CustodyReq;
import com.example.administrator.my.net.imp.NetUtil;
import com.example.administrator.my.statistic.MyPagerAdapter;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityCustodyBinding;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import utils.ToastUtil;

/**
 * 查看申请监护我的人的页面
 */
public class CustodyActivity extends Activity {
    private ActivityCustodyBinding binding;

    private MyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_custody);
        NetUtil.reqList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<CustodyReq>>() {
                    @Override
                    public void call(final List<CustodyReq> custodyReqs) {
                        if (null == custodyReqs) {
                            ToastUtil.toast(CustodyActivity.this, "当前无请求");
                            return;
                        }
                        adapter = new MyAdapter(custodyReqs);

                        binding.listView.setAdapter(adapter);

                        binding.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                                adapter.delete(position);
                            }
                        });
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        ToastUtil.toast(CustodyActivity.this, "出错了!");
                    }
                });
    }

    public void back(View view) {
        onBackPressed();
    }

    public class MyAdapter extends BaseAdapter {

        MyAdapter(List<CustodyReq> custodyReqs) {
            this.custodyReqs = custodyReqs;
        }

        private List<CustodyReq> custodyReqs;

        @Override
        public int getCount() {
            return custodyReqs.size();
        }

        @Override
        public CustodyReq getItem(int position) {
            return custodyReqs.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;
            if (null == convertView) {
                viewHolder = new ViewHolder();
                LayoutInflater mInflater = LayoutInflater.from(CustodyActivity.this);
                convertView = mInflater.inflate(R.layout.custody_item, null);
                viewHolder.nameTv = (TextView) convertView.findViewById(R.id.nameTv);
                viewHolder.timeTv = (TextView) convertView.findViewById(R.id.timeTv);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.nameTv.setText(getItem(position).getUserName());
            viewHolder.timeTv.setText(getItem(position).getDateTime());
            return convertView;
        }

        class ViewHolder {
            TextView nameTv;
            TextView timeTv;
        }

        public void delete(final int position) {
            AlertDialog.Builder builder = new AlertDialog.Builder(CustodyActivity.this);
            builder.setTitle("提示");
            builder.setMessage("是否同意" + custodyReqs.get(position).getUserName() + "的监护请求?");
            builder.setPositiveButton("同意", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    NetUtil.allow(custodyReqs.get(position).getId())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<Boolean>() {
                                @Override
                                public void call(Boolean aBoolean) {
                                    if (aBoolean) {
                                        ToastUtil.toast(CustodyActivity.this, "已同意");
                                        custodyReqs.remove(position);
                                        notifyDataSetChanged();
                                    } else {
                                        ToastUtil.toast(CustodyActivity.this, "请求失败");
                                    }
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    ToastUtil.toast(CustodyActivity.this, "网络错误");
                                }
                            });
                }
            });
            builder.setNegativeButton("拒绝", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    NetUtil.refuse(custodyReqs.get(position).getId())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<Boolean>() {
                                @Override
                                public void call(Boolean aBoolean) {
                                    if (aBoolean) {
                                        ToastUtil.toast(CustodyActivity.this, "已拒绝");
                                        custodyReqs.remove(position);
                                        notifyDataSetChanged();
                                    } else {
                                        ToastUtil.toast(CustodyActivity.this, "请求失败");
                                    }
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    ToastUtil.toast(CustodyActivity.this, "网络错误");
                                }
                            });
                }
            });
            builder.show();
        }
    }

    ;
}
