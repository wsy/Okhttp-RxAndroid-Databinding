package com.example.administrator.my.net.net;

import com.example.administrator.my.application;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetJson;
import utils.Preference;

/**
 * Created by wsy on 5/27/16.
 */
public class FallAddNet implements NetJson {
    private static final String URL = "http://" + NetConfig.IP + "/index.php?r=rest/v1/fall/create";

    private List<String> times;

    public FallAddNet(List<String> times) {
        this.times = times;
    }

    @Override
    public String getUrl() {
        return URL;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + Preference.getStr(application.getContext(), TokenNet.ACCESS_TOKEN, ""));
        headers.put("Content-Type", "application/json");
        return headers;
    }

    @Override
    public String getBody() {
        if (times != null && times.size() > 0) {
            try {
                JSONArray jsonArray = new JSONArray();
                for (String time : times) {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("dateTime", time);
                    jsonArray.put(jsonObject);

                }
                JSONObject result = new JSONObject();
                result.put("items", jsonArray);
                return result.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                return null;
            }
        }
        return null;
    }
}
