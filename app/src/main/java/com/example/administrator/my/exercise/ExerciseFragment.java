package com.example.administrator.my.exercise;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.administrator.my.db.AsynUtil;
import com.example.administrator.my.db.model.DbRecord;
import com.example.administrator.my.statistic.StatisticActivity;
import com.example.administrator.my.statistic.ViewPagerActivity;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.FragmentExerciseBinding;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ListExerciseHeaderBinding;
import com.trello.rxlifecycle.components.support.RxFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import utils.LogUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExerciseFragment extends RxFragment {
    private static final String TAG = ExerciseFragment.class.getSimpleName();

    public ExerciseFragment() {
        // Required empty public constructor
    }

    private ExerciseAdapter adapter;
    private ExerciseListView listView;
    private ExerciseDay exerciseDay;
    private List<ExerciseInfo> exerciseInfos;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        FragmentExerciseBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_exercise, container, false);
        listView = binding.listView;
        binding.btFresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fresh();
            }
        });

        // add header view
        final ListExerciseHeaderBinding headerBinding = DataBindingUtil.inflate(inflater, R.layout.list_exercise_header, listView, false);
        exerciseDay = new ExerciseDay("0", "0");
        headerBinding.setExerciseday(exerciseDay);
        headerBinding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), StatisticActivity.class);
                Intent intent = new Intent(getActivity(), ViewPagerActivity.class);
                startActivity(intent);
            }
        });

        listView.addHeaderView(headerBinding.getRoot());

        // add adapter
        exerciseInfos = new ArrayList<>();
        adapter = new ExerciseAdapter(exerciseInfos, getContext());
        listView.setAdapter(adapter);

        fresh();

        return binding.getRoot();
    }

    public void fresh() {
//        ToastUtil.toast(ExerciseFragment.this.getActivity(), "fresh data");

        // get the date
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        LogUtil.log(TAG, "" + calendar.getTime());

//        GetExerciseDayService
        // load title data
        AsynUtil.getExerciseDay(year, month, day)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ExerciseDay>() {
                    @Override
                    public void call(ExerciseDay exerciseDay) {
                        ExerciseFragment.this.exerciseDay.fresh(exerciseDay);
                    }
                });

//        GetDbRecordsService
        // load list data
        AsynUtil.getDbRecords(year, month, day)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<DbRecord>>() {
                    @Override
                    public void call(List<DbRecord> dbRecords) {
                        LogUtil.log(TAG, "record count: " + dbRecords.size());
                        exerciseInfos.clear();
                        for (DbRecord dbRecord : dbRecords) {
                            exerciseInfos.add(new ExerciseInfo(dbRecord));
                        }
                        adapter.notifyDataSetChanged();
                    }
                });
    }
}
