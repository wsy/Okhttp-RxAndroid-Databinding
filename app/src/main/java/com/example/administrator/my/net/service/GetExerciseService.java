package com.example.administrator.my.net.service;

import com.example.administrator.my.exercise.ExerciseDay;

import java.util.Date;
import java.util.List;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface GetExerciseService {
    public static final int DAY = 1;
    public static final int WEEK = 2;
    public static final int MONTH = 3;

    //传入一个继承了NetInterface的对象,然后返回一个Observable对象
    Observable<List<ExerciseDay>> get(Date date, int format);
}
