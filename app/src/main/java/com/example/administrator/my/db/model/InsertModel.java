package com.example.administrator.my.db.model;

import android.content.ContentValues;

/**
 * Created by wsy on 4/21/16.
 */
public interface InsertModel extends DbModel {
    ContentValues values();
}
