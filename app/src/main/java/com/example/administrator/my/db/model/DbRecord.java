package com.example.administrator.my.db.model;

import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import utils.DateUtil;

/**
 * Created by wsy on 4/26/16.
 */
public class DbRecord {
    public String getType() {
        return record.getType();
    }

    private Record record;

    public static final String ID = "id";
    private int id;
    public static final String ADDRESS = "address";
    private String address;
    public static final String HASUPDATE = "hasUpdate";
    public static final int NOTUPDATE = 0;
    private int hasUpdate;

    public boolean isThisDay(int year, int month, int day) {
        return DateUtil.isThatDay(Long.valueOf(record.getStartTime()), year, month, day);
    }

    public int steps() {
        return record.getSteps();
    }

    public boolean isSporting() {
        return record.isSporting();
    }

    public String getStarttimeStr() {
        return DateUtil.getTimeStr(Long.valueOf(record.getStartTime()));
    }

    public String getEndTimeStr() {
        return DateUtil.getTimeStr(Long.valueOf(record.getEndTime()));
    }

    public DbRecord(Cursor cursor) throws Exception {
        int idCo = cursor.getColumnIndex(ID);
        int addressCo = cursor.getColumnIndex(ADDRESS);
        int hasUpdateCo = cursor.getColumnIndex(HASUPDATE);
        if (-1 == idCo | -1 == addressCo | -1 == hasUpdateCo)
            throw new Exception("data lack in dbRecord");
        id = cursor.getInt(idCo);
        address = cursor.getString(addressCo);
        hasUpdate = cursor.getInt(hasUpdateCo);
        record = new Record(cursor);
    }

    public long time() {
        long startTime = Long.valueOf(record.getStartTime());
        long endTime = Long.valueOf(record.getEndTime());
        long gap = endTime - startTime;
        return gap;
    }

    public int getHasUpdate() {
        return hasUpdate;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public Record getRecord() {
        return record;
    }

    //    for update data to the server
    public JSONObject genJson() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("step", steps());
            jsonObject.put("startTime", getStarttimeStr());
            jsonObject.put("endTime", getEndTimeStr());
            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            return null;
        }
    }
}
