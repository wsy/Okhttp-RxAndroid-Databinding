package com.example.administrator.my.db.service;

import com.example.administrator.my.exercise.ExerciseDay;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface GetExerciseDayService {
    Observable<ExerciseDay> getExerciseDay(final int year, final int month, final int day);
}
