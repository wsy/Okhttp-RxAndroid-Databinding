package com.example.administrator.my.net.service;

import com.example.administrator.my.exercise.ExerciseDay;

import java.util.Date;
import java.util.List;

import rx.Observable;

/**
 * Created by wsy on 5/9/16.
 */
public interface GetSleepService {
    Observable<List<ExerciseDay>> get(Date date);
}
