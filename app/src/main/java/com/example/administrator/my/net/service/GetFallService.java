package com.example.administrator.my.net.service;

import java.util.List;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface GetFallService {
    Observable<List> get();
}
