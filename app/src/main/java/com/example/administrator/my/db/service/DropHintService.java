package com.example.administrator.my.db.service;

import com.example.administrator.my.hint.HintRecord;

import rx.Observable;

/**
 * Created by wsy on 5/9/16.
 */
public interface DropHintService {
    Observable drop(HintRecord hintRecord);
}
