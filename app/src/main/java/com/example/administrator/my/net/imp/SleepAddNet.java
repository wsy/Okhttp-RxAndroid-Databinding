package com.example.administrator.my.net.imp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by wsy on 5/27/16.
 */
public class SleepAddNet {
    private int total;
    private int awake;
    private int lightSleep;
    private int deepSleep;
    private int REM;
    private String startTime;
    private String endTime;

    public SleepAddNet(JSONObject jsonObject) {
        try {
            total = jsonObject.getInt("total");
            awake = jsonObject.getInt("awake");
            lightSleep = jsonObject.getInt("lightSleep");
            deepSleep = jsonObject.getInt("deepSleep");
            REM = jsonObject.getInt("REM");
            startTime = jsonObject.getString("startTime");
            endTime = jsonObject.getString("endTime");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getAwake() {
        return awake;
    }

    public int getDeepSleep() {
        return deepSleep;
    }

    public int getLightSleep() {
        return lightSleep;
    }

    public int getREM() {
        return REM;
    }

    public int getTotal() {
        return total;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStartTime() {
        return startTime;
    }
}

