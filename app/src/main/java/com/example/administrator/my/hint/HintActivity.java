package com.example.administrator.my.hint;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TimePicker;

import com.example.administrator.my.application;
import com.example.administrator.my.db.TableConfig;

import dbUtil.UseDatabase;

import com.example.administrator.my.db.model.DeleteModel;
import com.example.administrator.my.db.model.InsertModel;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityHintBinding;

import java.util.Calendar;

import utils.LogUtil;
import utils.ToastUtil;

public class HintActivity extends FragmentActivity {
    public static final String INTENT_HINT = "hint";

    private static final String TAG = HintActivity.class.getSimpleName();

    private HintRecord hintRecord;
    private HintRecord oldHintRecord;
    private ActivityHintBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hint);

        if (getIntent().hasExtra(INTENT_HINT)) {
            oldHintRecord = (HintRecord) getIntent().getSerializableExtra(INTENT_HINT);
            hintRecord = new HintRecord(oldHintRecord);
        } else {
            hintRecord = new HintRecord();
            binding.llDelete.setVisibility(View.GONE);
        }

        binding.setCommentStr(hintRecord.getHcomment());
        binding.setTimeStr(hintRecord.getTimeStr());
        binding.setDateStr(hintRecord.getDateStr());
        binding.setRepeatStr(hintRecord.getRepeatStr());
    }

    //确认按钮
    public void ensure(View view) {
        hintRecord.setHcomment(binding.etComment.getText().toString());

        // send broadcase & save to the database
        if (oldHintRecord != null) {
            deleteHint();
        }
        Intent intent = new Intent(this, HintReceiver.class);
        intent.setAction(HintReceiver.HINT_ACTION);
        intent.putExtra(HintReceiver.HINT_COMMENT, hintRecord.getHcomment());
        PendingIntent sender =
                PendingIntent.getBroadcast(this, hintRecord.getRequestCode(), intent, 0);
        Calendar calendar = Calendar.getInstance();
        long today = calendar.getTimeInMillis();
        calendar.set(Calendar.DAY_OF_WEEK, hintRecord.getHdate());
        calendar.set(Calendar.HOUR_OF_DAY, hintRecord.getHhour());
        calendar.set(Calendar.MINUTE, hintRecord.getHmin());
        calendar.set(Calendar.SECOND, 0);
        long aimday = calendar.getTimeInMillis();
        long gapTime = 0;
        if (hintRecord.getRepeat() == HintRecord.REPEAT)
            if (hintRecord.getHdate() == HintRecord.H_WEEK) {
                gapTime = 1000 * 60 * 60 * 24;
            } else {
                gapTime = 1000 * 60 * 60 * 24 * 7;
            }
        if (aimday < today) {
            if (hintRecord.getHdate() == HintRecord.H_WEEK) {
                calendar.add(Calendar.DAY_OF_WEEK, 1);
            } else {
                calendar.add(Calendar.DAY_OF_WEEK, 7);
            }
        }

        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        long startTime = calendar.getTimeInMillis();
        LogUtil.log(TAG, "start time: " + calendar.getTime());
        if (hintRecord.getRepeat() == HintRecord.REPEAT) {
            alarm.setRepeating(AlarmManager.RTC_WAKEUP
                    , startTime, gapTime, sender);
            LogUtil.log(TAG, "repeat: " + gapTime);
        } else {
            alarm.set(AlarmManager.RTC_WAKEUP, startTime, sender);
        }

//        trans into SaveHintService
        UseDatabase useDatabase = application.useDatabase;
        useDatabase.opendb();
//        useDatabase.executeSql("drop table hint");
        useDatabase.insert(new InsertModel() {
            @Override
            public ContentValues values() {
                return hintRecord.getValues();
            }

            @Override
            public String table() {
                return TableConfig.HINT;
            }
        });
        ToastUtil.toast(this, "设置成功");

        finish();
    }

    public void quit(View view) {
        onBackPressed();
    }

    //   choose time
    public void timePick(View view) {
        new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                hintRecord.setHhour(hourOfDay);
                hintRecord.setHmin(minute);
                binding.setTimeStr(hintRecord.getTimeStr());
            }
        }, hintRecord.getHhour(), hintRecord.getHmin(), true).show();
    }

    //    repeat set
    public void repeat(View view) {
        hintRecord.setRepeat();
        binding.setRepeatStr(hintRecord.getRepeatStr());
        binding.setDateStr(hintRecord.getDateStr());
    }

    //    date set
    public void date(View view) {
        new AlertDialog.Builder(this)
                .setTitle("请选择")
                .setIcon(android.R.drawable.ic_dialog_info)
                .setSingleChoiceItems(new String[]{"每天", "周日", "周一", "周二", "周三", "周四", "周五", "周六"}, hintRecord.getHdate(),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                hintRecord.setHdate(which);
                                binding.setDateStr(hintRecord.getDateStr());
                                dialog.dismiss();
                            }
                        }
                )
                .setNegativeButton("取消", null)
                .show();
    }

    //    delete
    public void delete(View view) {
        deleteHint();
        finish();
    }

    private void deleteHint() {
        UseDatabase useDatabase = application.useDatabase;
        Intent intent = new Intent(this, HintReceiver.class);
        intent.setAction(HintReceiver.HINT_ACTION);
        intent.putExtra(HintReceiver.HINT_COMMENT, oldHintRecord.getHcomment());
        PendingIntent sender =
                PendingIntent.getBroadcast(this, oldHintRecord.getRequestCode(), intent, 0);
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarm.cancel(sender);

//        DropHintService
        useDatabase.delete(new DeleteModel() {
            @Override
            public String whereClause() {
                return "id=?";
            }

            @Override
            public String[] whereArgs() {
                LogUtil.log(TAG, "hint id: " + oldHintRecord.getId());
                return new String[]{"" + oldHintRecord.getId()};
            }

            @Override
            public String table() {
                return TableConfig.HINT;
            }
        });
    }

}
