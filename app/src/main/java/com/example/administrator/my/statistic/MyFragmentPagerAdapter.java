package com.example.administrator.my.statistic;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Scroller;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import utils.LogUtil;

/**
 * Created by wsy on 5/3/16.
 */
public class MyFragmentPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {
    private Date today;
    public static final int OFFSET = 15;
    private static final String TAG = MyFragmentPagerAdapter.class.getSimpleName();
    private List<NewDayFragment> newDayFragments;
    private ViewPager viewPager;

    public MyFragmentPagerAdapter(FragmentManager fm, ViewPager viewPager) {
        super(fm);
        this.viewPager = viewPager;
        newDayFragments = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        today = calendar.getTime();
        LogUtil.log(TAG, "init date is : " + calendar.getTime());
        resetDate(today);
    }

    public void resetDate(Date date) {
        newDayFragments.clear();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, -1 * OFFSET - 1);
        for (int i = 0; i < OFFSET * 2 + 1; i++) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            if (calendar.getTime().getTime() > today.getTime()) {
                break;
            }
            LogUtil.log(TAG, "add date : " + calendar.getTime());
            newDayFragments.add(new NewDayFragment().setDate(calendar.getTime()));
        }
        notifyDataSetChanged();
        LogUtil.log(TAG, "current item date is : " + newDayFragments.get(OFFSET).getDate());
        viewPager.setCurrentItem(OFFSET, false);
    }

    public Date getNowDate() {
        return date;
    }

    private Date date;

    @Override
    public Fragment getItem(int position) {
        return newDayFragments.get(position);
    }

//    @Override
//    public int getItemPosition(Object object) {
//        return POSITION_NONE;
//    }

    @Override
    public int getCount() {
        return newDayFragments.size();
    }

    //    OnPageChangeListener

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(final int position) {
        date = newDayFragments.get(position).getDate();
        if (date.getTime() == today.getTime())
            return;
        if (0 == position || newDayFragments.size() - 1 == position) {
            resetDate(date);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
