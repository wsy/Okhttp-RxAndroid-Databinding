package com.example.administrator.my.device;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityCommunicationBinding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import bluetoothUtil.BluetoothUtil;

/**
 * Created by wsy on 4/21/16.
 * <p>
 * 用于测试和硬件的通讯;以后可以改成看设备当前的状态的页面
 *
 * @deprecated
 */
public class CommunicationActivity extends AppCompatActivity {
    private static final String TAG = CommunicationActivity.class.getSimpleName();
    private ActivityCommunicationBinding binding;
    private List<String> infos;
    private String address;
    private String psw;
//    private String uuid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_communication);
        address = getIntent().getStringExtra("address");
        psw = getIntent().getStringExtra("psw");
//        uuid = getIntent().getStringExtra("uuid");
        infos = new ArrayList<>();
        binding.listView.setAdapter(adapter);
        new Thread() {
            @Override
            public void run() {
                Log.i(TAG, "start device thread");
                communicate();
            }
        }.start();
    }

    private BaseAdapter adapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return infos.size();
        }

        @Override
        public String getItem(int position) {
            return infos.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView textView = new TextView(CommunicationActivity.this);
            textView.setText(getItem(position));
            AbsListView.LayoutParams layout = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            textView.setLayoutParams(layout);
            return textView;
        }
    };

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            infos.add(0, msg.getData().getString("info"));
            adapter.notifyDataSetChanged();
        }
    };

    private void communicate() {
        Log.i(TAG, "address: " + address);
        Log.i(TAG, "psw: " + psw);
        BluetoothDevice device = BluetoothUtil.pair(address, psw);
        if (device != null) {
            Log.i(TAG, "device pair success");
            BluetoothSocket socket;
            OutputStream outputStream;
            PrintWriter writer;
            BufferedReader reader;
            InputStream inputStream = null;
            try {
                socket = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                Log.i(TAG, "socket create success");
                socket.connect();
                Log.i(TAG, "socket connect success");
                outputStream = socket.getOutputStream();
                writer = new PrintWriter(outputStream);
                inputStream = socket.getInputStream();
                // test write
                reader = new BufferedReader(new InputStreamReader(inputStream));
                writer.write("hello world" + (char) 31);
                Log.i(TAG, "send message success");
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
                Bundle bundle = new Bundle();
                bundle.putString("info", "socket create or connect fail");
                Message msg = new Message();
                msg.setData(bundle);
                handler.sendMessage(msg);
                return;
            }
            StringBuffer sb = new StringBuffer();
            char c;
            while (true) {
                try {
                    c = (char) reader.read();
                    if (c != -1) {
                        if (31 == c) {
                            sendInfo(sb.toString());
                            sb = new StringBuffer();
                        } else {
                            sb.append(c);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.i(TAG, "input stream read error");
                }
            }

        } else {
            Log.i(TAG, "device pair fail");
        }
    }

    private void sendInfo(String info) {
        Log.i(TAG, info);
        Bundle bundle = new Bundle();
        bundle.putString("info", info);
        Message msg = new Message();
        msg.setData(bundle);
        handler.sendMessage(msg);
    }
}
