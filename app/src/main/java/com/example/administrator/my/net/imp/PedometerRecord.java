package com.example.administrator.my.net.imp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by wsy on 5/27/16.
 */
public class PedometerRecord {
    private int id;
    private String userName;
    private int step;
    private String dateTime;

    public PedometerRecord(JSONObject jsonObject) {
        try {
            id = jsonObject.getInt("id");
            userName = jsonObject.getString("userName");
            step = jsonObject.getInt("step");
            dateTime = jsonObject.getString("dateTime");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public int getStep() {
        return step;
    }

    public String getDateTime() {
        return dateTime;
    }
}
