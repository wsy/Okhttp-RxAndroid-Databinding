package com.example.administrator.my.home;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.administrator.my.device.DeviceActivity;
import com.example.administrator.my.device.DeviceConfig;
import com.example.administrator.my.device.ReceiveThread;
import com.example.administrator.my.exercise.ExerciseFragment;
import com.example.administrator.my.hint.HintFragment;
import com.example.administrator.my.person.PersonActivity;
import com.example.administrator.my.sleep.SleepFragment;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityHomeBinding;

import java.util.ArrayList;
import java.util.List;

import utils.LogUtil;
import utils.Preference;
import utils.ToastUtil;


public class HomeActivity extends FragmentActivity {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private ActivityHomeBinding binding;
    private FragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        initAdapter();
        binding.viewPager.setAdapter(adapter);

        // check if not bind device ,jump to the device activity
        String address = Preference.getStr(HomeActivity.this, DeviceConfig.DEVICE_ADDRESS, "");
        String addressHint = "当前无绑定设备";
        if (address.trim().equals("")) {
            Intent intent = new Intent(HomeActivity.this, DeviceActivity.class);
            startActivity(intent);
        } else {
            addressHint = address;
            new ReceiveThread(address).start();
        }
        binding.setAddress(addressHint);

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(receiver);
        super.onDestroy();
    }

    private boolean isConnect = false;
    public BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            BluetoothDevice device = intent
                    .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            if (null == device)
                return;
            if (!device.getAddress().equals(Preference.getStr(HomeActivity.this, DeviceConfig.DEVICE_ADDRESS, "")))
                return;
            String action = intent.getAction();
            switch (action) {
                case BluetoothDevice.ACTION_ACL_CONNECTED:
                    isConnect = true;
                    binding.tvAddress.setBackgroundResource(R.drawable.rect_green);
                    break;
                case BluetoothDevice.ACTION_ACL_DISCONNECTED:
                    binding.tvAddress.setBackgroundResource(R.drawable.rect_grey);
                    isConnect = false;
                    break;
            }
        }
    };

    public void click_connect(View view) {
        if (isConnect)
            return;
        String address = Preference.getStr(HomeActivity.this, DeviceConfig.DEVICE_ADDRESS, "");
        if (address.trim().equals("")) {
            Intent intent = new Intent(HomeActivity.this, DeviceActivity.class);
            startActivity(intent);
        } else {
            new ReceiveThread(address).start();
        }
    }

    public void click_set(View view) {
        Intent intent = new Intent(this, PersonActivity.class);
        startActivity(intent);
    }

    private void initAdapter() {
        final List<Fragment> fragmentList = new ArrayList<Fragment>();
        fragmentList.add(new ExerciseFragment());
        fragmentList.add(new SleepFragment());
        fragmentList.add(new HintFragment());
        adapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }
        };
    }


}
