package com.example.administrator.my.net.service;

import rx.Observable;
import rx.internal.util.ObjectPool;

/**
 * Created by wsy on 5/7/16.
 */
public interface LoginService {
    Observable<Object> login(String account, String password);
}
