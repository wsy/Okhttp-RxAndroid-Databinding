package com.example.administrator.my.db.service;

import com.example.administrator.my.db.model.DbRecord;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface UpdateRecordService {
    Observable<Boolean> updateRecord(final DbRecord dbRecord);
}
