package com.example.administrator.my.db.service;

import com.example.administrator.my.db.model.DbRecord;
import com.example.administrator.my.db.model.Record;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface DropRecordService {
    Observable drop(DbRecord dbRecord);
}
