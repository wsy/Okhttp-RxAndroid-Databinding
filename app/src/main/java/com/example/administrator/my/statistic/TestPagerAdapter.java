package com.example.administrator.my.statistic;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
//import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by wsy on 5/4/16.
 * @deprecated
 */
public class TestPagerAdapter extends PagerAdapter implements MyViewPager.OnPageChangeListener {
    private static final String TAG = PagerAdapter.class.getSimpleName();

    private List<NewDayFragment> newDayFragments;

    private WeakReference<Context> weakReference;
    private MyViewPager viewPager;

    public TestPagerAdapter(Context context, MyViewPager viewPager) {
        weakReference = new WeakReference<Context>(context);
        this.viewPager = viewPager;

        newDayFragments = new ArrayList<>();
        newDayFragments.clear();
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date beforeDay = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        Date laterDay = calendar.getTime();
        newDayFragments.add(new NewDayFragment().setDate(beforeDay));
        newDayFragments.add(new NewDayFragment().setDate(date));
        newDayFragments.add(new NewDayFragment().setDate(laterDay));
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return newDayFragments.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        MyViewPager viewPager = (MyViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
        view = null;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(weakReference.get());
        View view = newDayFragments.get(position).onCreateView(inflater, container, null);
        return view;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (0 == position) {
            Date date = newDayFragments.get(position).getDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, -1);
            newDayFragments.add(0, new NewDayFragment().setDate(calendar.getTime()));
//            viewPager.setFuck();
            notifyDataSetChanged();
        } else if (newDayFragments.size() - 1 == position) {
            Date date = newDayFragments.get(position).getDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            newDayFragments.add(position + 1, new NewDayFragment().setDate(calendar.getTime()));
            notifyDataSetChanged();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
