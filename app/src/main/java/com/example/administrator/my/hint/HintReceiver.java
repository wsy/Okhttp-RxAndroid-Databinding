package com.example.administrator.my.hint;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import bluetoothUtil.BluetoothUtil;
import com.example.administrator.my.device.DeviceConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.UUID;

import utils.LogUtil;
import utils.Preference;
import utils.SysUtil;

public class HintReceiver extends BroadcastReceiver {
    private static final String TAG = HintReceiver.class.getSimpleName();

    // the same to the manifest.xml
    public static final String HINT_ACTION = "hint";

    //    for send message key
    public static final String HINT_COMMENT = "comment";

    public HintReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtil.log(TAG, intent.getAction());
        if (intent.getAction().equals(HINT_ACTION)) {
            SysUtil.notify(context, intent.getStringExtra(HINT_COMMENT));

            String address = Preference.getStr(context, DeviceConfig.DEVICE_ADDRESS, "");
            LogUtil.log(TAG, "address: " + address);
            if (address.trim().equals("")) {
                return;
            }
            BluetoothDevice device = BluetoothUtil.pair(address, "1234");
            if (device != null) {
                try {
                    BluetoothSocket socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                    OutputStream outputStream = socket.getOutputStream();
                    PrintWriter printWriter = new PrintWriter(outputStream);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("type", "choke");
                    jsonObject.put("time", -1);
                    jsonObject.put("vibration", true);
                    jsonObject.put("beep", true);
                    printWriter.write(jsonObject.toString());
//                    printWriter.flush();
//                    printWriter.close();
                    LogUtil.log(TAG, "send success");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
