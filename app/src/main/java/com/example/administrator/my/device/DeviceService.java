package com.example.administrator.my.device;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.IBinder;

import utils.LogUtil;
import utils.Preference;

/**
 * Created by wsy on 4/21/16.
 * <p/>
 * 用于在后台与设备进行数据传输,主要是接收数据
 */
public class DeviceService extends Service {
    private static final String TAG = DeviceService.class.getSimpleName();
    public static final String MESSAGE = "message";
    public static final int START_FOUND = 1;
    public static final int STOP_FOUND = 2;

    private String address = null;

    public DeviceService() {
        LogUtil.log(TAG, "new DeviceService()");
    }

    private ConnectThread connectThread;

    //startService callback
    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        LogUtil.log(TAG, "onStart");
        // test db
//        new TestThread().start();
        if (null == intent) {
            LogUtil.log(TAG, "intent is null");
            return;
        }
        switch (intent.getIntExtra(MESSAGE, 0)) {
            case START_FOUND:
                if (null == connectThread)
                    connectThread = new ConnectThread();
                if (Thread.State.RUNNABLE != connectThread.getState())
                    connectThread.start();
                break;
            case STOP_FOUND:
                if (connectThread != null)
                    connectThread.interrupt();
                break;
            default:
                break;
        }
        new ConnectThread().start();
    }

    // bindService callback
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        LogUtil.log(TAG, "onBind");
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        LogUtil.log(TAG, "onDestroy");
        super.onDestroy();
        if (connectThread != null && connectThread.isAlive())
            connectThread.interrupt();
        if (receiveThread != null && receiveThread.isAlive()) {
            receiveThread.interrupt();
        }
    }

    private ReceiveThread receiveThread;


    public class ConnectThread extends Thread {
        private final String TAG = ConnectThread.class.getName();

        @Override
        public void run() {
            LogUtil.log(TAG, "connection thread run");
            do {
                String address = Preference.getStr(DeviceService.this, DeviceConfig.DEVICE_ADDRESS, "");
                // 如果发现至今还没有连接过设备,就直接关闭尝试连接设备的线程
                if (address.trim().equals("")) {
                    LogUtil.log(TAG, "no device bond");
                    return;
                }
                if (receiveThread != null && Thread.State.RUNNABLE == receiveThread.getState()) {
                    LogUtil.log(TAG, "receive thread is alive");
                    return;
                }
                BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
                LogUtil.log(TAG, "device bond state: " + device.getBondState());
                if (!BluetoothDevice.ACTION_ACL_CONNECTED.equals(device.getBondState())) {
                    if (null == receiveThread)
                        receiveThread = new ReceiveThread(address);
                    receiveThread.start();
                }

                try {
                    Thread.sleep(5 * 60 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (true);
        }
    }
}
