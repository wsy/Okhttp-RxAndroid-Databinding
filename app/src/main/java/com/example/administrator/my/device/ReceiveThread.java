package com.example.administrator.my.device;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ContentValues;

import com.example.administrator.my.application;
import com.example.administrator.my.db.TableConfig;

import bluetoothUtil.BluetoothUtil;
import dbUtil.UseDatabase;
import com.example.administrator.my.db.model.InsertModel;
import com.example.administrator.my.db.model.Record;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.UUID;

import utils.LogUtil;
import utils.SysUtil;

/**
 * Created by wsy on 4/21/16.
 *
 */
public class ReceiveThread extends Thread {
    private static final String TAG = ReceiveThread.class.getSimpleName();
    private String address;

    public ReceiveThread(String address) {
        this.address = address;
    }

    @Override
    public void run() {
        LogUtil.log(TAG, "address: " + address);
        LogUtil.log(TAG, "psw: " + "1234");
        BluetoothDevice device = BluetoothUtil.pair(address, "1234");
        if (device != null) {
            LogUtil.log(TAG, "device pair success");
            BluetoothSocket socket;
            BufferedReader reader = null;
            InputStream inputStream = null;
            try {
                socket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                LogUtil.log(TAG, "socket create success");
                socket.connect();
                LogUtil.log(TAG, "socket connect success");
                inputStream = socket.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream));

                // 校对时间戳
                OutputStream outputStream = socket.getOutputStream();
                PrintWriter printWriter = new PrintWriter(outputStream);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("type", "timesync");
                jsonObject.put("time", System.currentTimeMillis() / 1000);
                jsonObject.put("vibration", true);
                jsonObject.put("beep", true);

                LogUtil.log(TAG, jsonObject.toString());
                printWriter.write(jsonObject.toString());
                printWriter.flush();

            } catch (IOException e) {
                e.printStackTrace();
                try {
                    if (reader != null)
                        reader.close();
                    if (inputStream != null)
                        inputStream.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                return;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            StringBuffer sb = new StringBuffer();
            char c;
            while (true) {
                try {
                    c = (char) reader.read();
                    if (c != -1) {
                        if (31 == c) {
                            String info = sb.toString();// one json record
                            LogUtil.log(TAG, info);
                            copeInfo(info);
                            sb = new StringBuffer();
                        } else {
                            sb.append(c);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        reader.close();
                        inputStream.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    LogUtil.log(TAG, "input stream read error");
                    return;
                }
            }
        } else {
            LogUtil.log(TAG, "device pair fail");
        }
    }

    // 处理接收到的一条记录
    private void copeInfo(String jsonInfo) {
        try {
            JSONObject jsonObject = new JSONObject(jsonInfo);
            final Record record = new Record(jsonObject);
            if (record.isSporting() && record.getSteps() == 0) {
                LogUtil.log(TAG, "drop : " + jsonInfo);
                return;
            }
            if (record.getType().equals(Record.TYPE_FALL)) {
                SysUtil.notify(application.getContext(), "摔倒了");
            }

//            SaveRecordService
            UseDatabase useDatabase = application.useDatabase;
            useDatabase.opendb();
            useDatabase.insert(new InsertModel() {
                @Override
                public ContentValues values() {
                    record.getValues().put(TableConfig.ADDRESS, address);
                    record.getValues().put(TableConfig.HASUPDATE, 0);
                    return record.getValues();
                }

                @Override
                public String table() {
                    return TableConfig.RECORD;
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            LogUtil.log(TAG, "json info format error");
        }
    }
}
