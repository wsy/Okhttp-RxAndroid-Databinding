package com.example.administrator.my.net.net;

import com.example.administrator.my.application;
import com.example.administrator.my.net.imp.FallRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetInterface;
import utils.Preference;

/**
 * get
 * <p/>
 * Created by wsy on 5/27/16.
 */
public class FallGetNet implements NetInterface {
    private static final String URL = "http://" + NetConfig.IP + "/index.php?r=rest/v1/fall/view&dateTime=";

    private String time;

    public FallGetNet(String time) {
        this.time = time;
    }

    @Override
    public String getURL() {
        return URL + time;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + Preference.getStr(application.getContext(), TokenNet.ACCESS_TOKEN, ""));
        return headers;
    }

    @Override
    public Map<String, String> getParams() {
        return null;
    }

    public List<FallRecord> decodeResult(String json) {
        JSONObject jsonObject = new JSONObject();
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            List<FallRecord> fallRecords = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                fallRecords.add(new FallRecord(jsonArray.getJSONObject(i)));
            }
            return fallRecords;
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            return null;
        }
    }

}
