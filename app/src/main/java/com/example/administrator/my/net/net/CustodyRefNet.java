package com.example.administrator.my.net.net;

import java.util.HashMap;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetInterface;
import utils.Preference;

/**
 * Created by wsy on 5/29/16.
 * 6.4. 拒绝请求
 */
public class CustodyRefNet implements NetInterface {
    private static final String URL = "http://"+ NetConfig.IP+"/index.php?r=rest/v1/guardianship-request/refuse&id=";

    private int id;

    public CustodyRefNet(int id) {
        this.id = id;
    }

    @Override
    public String getURL() {
        return URL + id;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + Preference.getStr(TokenNet.ACCESS_TOKEN));
        return headers;
    }

    @Override
    public Map<String, String> getParams() {
        return null;
    }

    public static boolean decodeResult(String json) {
        return CustodyAllowNet.decodeResult(json);
    }
}
