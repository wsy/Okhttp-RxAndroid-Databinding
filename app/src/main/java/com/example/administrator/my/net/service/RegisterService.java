package com.example.administrator.my.net.service;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface RegisterService {
    Observable<Object> register(String account, String password);
}
