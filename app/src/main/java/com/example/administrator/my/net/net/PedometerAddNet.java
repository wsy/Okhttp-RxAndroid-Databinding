package com.example.administrator.my.net.net;

import com.example.administrator.my.application;
import com.example.administrator.my.db.model.DbRecord;
import com.example.administrator.my.db.model.Record;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetJson;
import utils.Preference;

/**
 * Created by wsy on 5/27/16.
 */
public class PedometerAddNet implements NetJson {
    private static final String URL = "http://" + NetConfig.IP + "/index.php?r=rest/v1/step/create";

    private List<DbRecord> records;

    public PedometerAddNet(List<DbRecord> recordList) {
        this.records = recordList;
    }

    @Override
    public String getUrl() {
        return URL;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + Preference.getStr(application.getContext(), TokenNet.ACCESS_TOKEN, ""));
        headers.put("Content-Type", "application/json");
        return headers;
    }

    @Override
    public String getBody() {
        if (records != null && records.size() > 0) {
            JSONArray jsonArray = new JSONArray();
            for (DbRecord dbRecord : records) {
                jsonArray.put(dbRecord.genJson());
            }
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("items", jsonArray);
                return jsonObject.toString();
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }
}
