package com.example.administrator.my.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.administrator.my.application;
import com.example.administrator.my.db.model.DbRecord;
import com.example.administrator.my.db.model.UpdateModel;
import com.example.administrator.my.exercise.ExerciseDay;
import com.example.administrator.my.hint.HintRecord;

import java.util.ArrayList;
import java.util.List;

import dbUtil.UseDatabase;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;
import utils.DateUtil;
import utils.LogUtil;

/**
 * Created by wsy on 4/25/16.
 */
public class AsynUtil {
    private static final String TAG = AsynUtil.class.getSimpleName();

    public static Observable<Boolean> updateRecord(final DbRecord dbRecord) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(Subscriber<? super Boolean> subscriber) {
                UseDatabase useDatabase = application.useDatabase;
                int result = useDatabase.update(new UpdateModel() {
                    @Override
                    public String whereClause() {
                        return "id=?";
                    }

                    @Override
                    public String[] whereArgs() {
                        return new String[]{"" + dbRecord.getId()};
                    }

                    @Override
                    public ContentValues values() {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DbRecord.HASUPDATE, 1);
                        return contentValues;
                    }

                    @Override
                    public String table() {
                        return TableConfig.RECORD;
                    }
                });
                subscriber.onNext(1 == result ? true : false);
            }
        });
    }

    public static Observable<List<HintRecord>> getHints() {
        return Observable.create(new Observable.OnSubscribe<List<HintRecord>>() {
            @Override
            public void call(Subscriber<? super List<HintRecord>> subscriber) {
                UseDatabase useDatabase = application.useDatabase;
                Cursor cursor = useDatabase.rawQuery("select * from hint", null);
                List<HintRecord> hintRecords = new ArrayList<HintRecord>();
                while (cursor.moveToNext()) {
                    try {
                        hintRecords.add(new HintRecord(cursor));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                subscriber.onNext(hintRecords);
            }
        });
    }

    public static Observable<ExerciseDay> getExerciseDay(final int year, final int month, final int day) {
        return getDbRecords(year, month, day).flatMap(new Func1<List<DbRecord>, Observable<ExerciseDay>>() {
            @Override
            public Observable<ExerciseDay> call(List<DbRecord> dbRecords) {
                int runSteps = 0;
                long time = 0;
                for (DbRecord dbRecord : dbRecords) {
                    if (dbRecord.isThisDay(year, month, day) && dbRecord.isSporting()) {
                        runSteps += dbRecord.steps();
                        time += dbRecord.time();
                    }
                }
                final ExerciseDay exerciseDay = new ExerciseDay("" + runSteps, DateUtil.gapToStr(time));
                return Observable.create(new Observable.OnSubscribe<ExerciseDay>() {
                    @Override
                    public void call(Subscriber<? super ExerciseDay> subscriber) {
                        subscriber.onNext(exerciseDay);
                    }
                });
            }
        });
    }

    // 获得数据库中所有的记录
    public static Observable<List<DbRecord>> getDbRecords(final int year, final int month, final int day) {
        return Observable.create(new Observable.OnSubscribe<List<DbRecord>>() {
            @Override
            public void call(Subscriber<? super List<DbRecord>> subscriber) {
                UseDatabase useDatabse = application.useDatabase;
                Cursor cursor = useDatabse.rawQuery("select * from record", null);
                List<DbRecord> dbRecords = new ArrayList<DbRecord>();
                while (cursor.moveToNext()) {
                    try {
                        DbRecord dbRecord = new DbRecord(cursor);
                        if (dbRecord.isThisDay(year, month, day))
                            dbRecords.add(new DbRecord(cursor));
                    } catch (Exception e) {
                        e.printStackTrace();
                        for (String name : cursor.getColumnNames()) {
                            LogUtil.log(TAG, "column name: " + name);
                        }
                    }
                }
//                LogUtil.log(TAG, "record count is : " + dbRecords.size());
                subscriber.onNext(dbRecords);
            }
        });
    }

    public static Observable<List<DbRecord>> getAllDbRecords() {
        return Observable.create(new Observable.OnSubscribe<List<DbRecord>>() {
            @Override
            public void call(Subscriber<? super List<DbRecord>> subscriber) {
                UseDatabase useDatabse = application.useDatabase;
                Cursor cursor = useDatabse.rawQuery("select * from record", null);
                List<DbRecord> dbRecords = new ArrayList<DbRecord>();
                while (cursor.moveToNext()) {
                    try {
                        DbRecord dbRecord = new DbRecord(cursor);
                        dbRecords.add(new DbRecord(cursor));
                    } catch (Exception e) {
                        e.printStackTrace();
                        for (String name : cursor.getColumnNames()) {
                            LogUtil.log(TAG, "column name: " + name);
                        }
                    }
                }
                LogUtil.log(TAG, "record count is : " + dbRecords.size());
                subscriber.onNext(dbRecords);
            }
        });
    }
}
