package com.example.administrator.my.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.administrator.okhttp_rxandroid_databinding.R;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }
}
