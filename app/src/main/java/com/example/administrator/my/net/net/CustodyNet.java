package com.example.administrator.my.net.net;

import com.example.administrator.my.application;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetJson;
import utils.LogUtil;
import utils.Preference;

/**
 * Created by wsy on 5/29/16.
 * 6.1. 提交监护某用户的请求
 */
public class CustodyNet implements NetJson {
    private static final String URL = "http://" + NetConfig.IP + "/index.php?r=rest/v1/guardianship-request/create";

    private String guardianUserName;

    public CustodyNet(String guardianUserName) {
        this.guardianUserName = guardianUserName;
    }

    @Override
    public String getUrl() {
        return URL;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + Preference.getStr(TokenNet.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }

    @Override
    public String getBody() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userName", "");
            jsonObject.put("guardianUserName", guardianUserName);
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonObject);
            JSONObject result = new JSONObject();
            result.put("items", jsonArray);
            return result.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean decodeResult(String json) {
        try {
            JSONArray jsonArray = new JSONArray(json);
            JSONObject jsonObject = jsonArray.getJSONObject(0);
            int state = jsonObject.getInt("state");
            return 1 == state ? true : false;
        } catch (JSONException e) {
            e.printStackTrace();
            LogUtil.log(CustodyNet.class, json);
            return false;
        }
    }
}
