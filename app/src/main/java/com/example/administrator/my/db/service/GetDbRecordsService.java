package com.example.administrator.my.db.service;

import com.example.administrator.my.db.model.DbRecord;

import java.util.List;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface GetDbRecordsService {
    Observable<List<DbRecord>> getDbRecords(final int year, final int month, final int day);
}
