package com.example.administrator.my.net.net;

import java.util.HashMap;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetInterface;
import utils.StringUtil;

/**
 * Created by wsy on 4/28/16.
 *
 * @deprecated
 */
public class UploadFallNet implements NetInterface {
    private String userId;
    private String userName;
    private String dateTime;

    public UploadFallNet(String userId, String userName, String dateTime) {
        this.userId = userId;
        this.userName = userName;
        this.dateTime = dateTime;
    }

    private static final String URL = "http://" + NetConfig.IP + "/index.php?r=rest/v1/fall/create";

    @Override
    public String getURL() {
        return URL;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
//        headers.put("Authorization", "Basic " + StringUtil.getBase64(NetConfig.TOKEN));
        return headers;
    }

    @Override
    public Map<String, String> getParams() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("userId", userId);
        params.put("userName", userName);
        params.put("dateTime", dateTime);
        return params;
    }
}
