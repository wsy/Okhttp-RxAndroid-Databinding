package com.example.administrator.my.net.imp;

import org.json.JSONException;
import org.json.JSONObject;

import utils.LogUtil;

/**
 * Created by wsy on 5/29/16.
 */
public class CustodyReq {
    private int id;
    private String userName;
    private String guardianUserName;
    private String dateTime;

    public CustodyReq(JSONObject jsonObject) {
        try {
            id = jsonObject.getInt("id");
            userName = jsonObject.getString("userName");
            guardianUserName = jsonObject.getString("guardianUserName");
        } catch (JSONException e) {
            e.printStackTrace();
            LogUtil.log(CustodyReq.class, jsonObject.toString());
        }
        try {
            dateTime = jsonObject.getString("dateTime");
        } catch (JSONException e) {
            e.printStackTrace();
            dateTime = "";
        }
    }

    public String getDateTime() {
        return dateTime;
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getGuardianUserName() {
        return guardianUserName;
    }
}
