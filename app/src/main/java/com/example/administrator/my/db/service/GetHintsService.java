package com.example.administrator.my.db.service;

import com.example.administrator.my.hint.HintRecord;

import java.util.List;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface GetHintsService {
    Observable<List<HintRecord>> getHints();
}
