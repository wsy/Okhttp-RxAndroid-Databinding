package com.example.administrator.my.exercise;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.administrator.okhttp_rxandroid_databinding.BR;

import utils.DateUtil;

/**
 * Created by wsy on 2016/3/24.
 */
public class ExerciseDay extends BaseObservable {
    private String run_steps;
    private String sport_time;
    private String timeStr;

    public ExerciseDay(String run_steps, String sport_time) {
        this.run_steps = run_steps;
        this.sport_time = sport_time;
        timeStr = DateUtil.getTimeStr(System.currentTimeMillis());
    }

    public void fresh(ExerciseDay exerciseDay) {
        setRun_steps(exerciseDay.getRun_steps());
        setSport_time(exerciseDay.getSport_time());
    }

    @Bindable
    public String getRun_steps() {
        return run_steps;
    }

    public void setRun_steps(String run_steps) {
        this.run_steps = run_steps;
        notifyPropertyChanged(BR.run_steps);
    }

    @Bindable
    public String getSport_time() {
        return sport_time;
    }

    public void setSport_time(String sport_time) {
        this.sport_time = sport_time;
        notifyPropertyChanged(BR.sport_time);
    }
}
