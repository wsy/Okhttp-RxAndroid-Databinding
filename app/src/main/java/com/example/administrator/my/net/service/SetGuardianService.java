package com.example.administrator.my.net.service;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface SetGuardianService {
    public static final int SET = 1;
    public static final int UNSET = 0;

    Observable<Object> set(String userId, int format);
}
