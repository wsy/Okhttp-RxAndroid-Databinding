package com.example.administrator.my.net.net;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetJson;

/**
 * Created by wsy on 5/27/16.
 */
public class RefreshTokenNet implements NetJson {
    private static final String URL = "http://" + NetConfig.IP + "/index.php?r=oauth/token";

    private String refresh_token;

    public RefreshTokenNet(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    @Override
    public String getUrl() {
        return URL;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Basic aXVNVkk4eEZidnJjZHBackRRRFZoZG1jTG1JdDVEZ0lpdU1WSTh4RmJ2cmNkcFpyRFFEVmhkbWNMbUl0NURnSWl1TVZJOHhGYnZyY2RwWnJEUURWaGRtY0xtSXQ1RGdsOg==");
        headers.put("Content-Type", "application/json");
        return headers;
    }

    public static final String GRANT_TYPE = "grant_type";

    @Override
    public String getBody() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(GRANT_TYPE, "refresh_token");
            jsonObject.put(TokenNet.REFRESH_TOKEN, refresh_token);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            return null;
        }
    }

    public boolean decodeResult(String json) {
        return TokenNet.decodeResult(json);
    }
}
