package com.example.administrator.my.net.net;

import com.example.administrator.my.application;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetJson;
import utils.LogUtil;
import utils.Preference;

/**
 * Created by wsy on 5/27/16.
 */
public class TokenNet implements NetJson {
    public static final String URL = "http://" + NetConfig.IP + "/index.php?r=oauth/token";

    private String code;

    public TokenNet(String code) {
        this.code = code;
    }

    @Override
    public String getUrl() {
        return URL;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
//        headers.put("Authorization", "Basic aXVNVkk4eEZidnJjZHBackRRRFZoZG1jTG1JdDVEZ0lpdU1WSTh4RmJ2cmNkcFpyRFFEVmhkbWNMbUl0NURnSWl1TVZJOHhGYnZyY2RwWnJEUURWaGRtY0xtSXQ1RGdsOg==");
        headers.put("Authorization", "Basic dGVzdGNsaWVudDp0ZXN0cGFzcw==");
        headers.put("Content-Type", "application/json");
        return headers;
    }

    @Override
    public String getBody() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("grant_type", "authorization_code");
            jsonObject.put("code", code);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static final String ACCESS_TOKEN = "access_token";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String ERROR = "error";

    public static boolean decodeResult(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            if (jsonObject.has("access_token")) {
                Preference.putStr(application.getContext(), ACCESS_TOKEN, jsonObject.getString(ACCESS_TOKEN));
                Preference.putStr(application.getContext(), REFRESH_TOKEN, jsonObject.getString(REFRESH_TOKEN));
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            LogUtil.log(TokenNet.class, json);
            return false;
        }
        return false;
    }
}
