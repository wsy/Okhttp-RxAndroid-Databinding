package com.example.administrator.my.net.net;

import com.example.administrator.my.net.imp.CustodyReq;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import netUtil.NetConfig;
import netUtil.NetInterface;
import utils.Preference;

/**
 * Created by wsy on 5/29/16.
 * 6.2. 查询申请监护该用户的监护请求
 */
public class CustodyReqNet implements NetInterface {

    private static final String URL = "http://" + NetConfig.IP + "/index.php?r=rest/v1/guardianship-request/view";

    @Override
    public String getURL() {
        return URL;
    }

    @Override
    public Map<String, String> getHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + Preference.getStr(TokenNet.ACCESS_TOKEN));
        return headers;
    }

    @Override
    public Map<String, String> getParams() {
        return null;
    }

    public static List<CustodyReq> decodeResult(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("items");
            List<CustodyReq> custodyReqs = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                custodyReqs.add(new CustodyReq(jsonArray.getJSONObject(i)));
            }
            return custodyReqs;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
