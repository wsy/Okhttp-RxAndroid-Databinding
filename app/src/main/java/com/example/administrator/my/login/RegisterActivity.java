package com.example.administrator.my.login;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.example.administrator.my.home.HomeActivity;
import com.example.administrator.my.net.imp.NetUtil;
import com.example.administrator.okhttp_rxandroid_databinding.R;
import com.example.administrator.okhttp_rxandroid_databinding.databinding.ActivityLoginBinding;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import utils.ToastUtil;

public class RegisterActivity extends Activity {
    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
    }

    public void registerAccount(View view) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("注册");
        progressDialog.setMessage("请耐心等待");
        progressDialog.show();
        NetUtil.register(binding.account.getText().toString(), binding.password.getText().toString())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        progressDialog.dismiss();
                        if (aBoolean) {
                            ToastUtil.toast(RegisterActivity.this, "注册成功");
                            finish();
                        } else {
                            ToastUtil.toast(RegisterActivity.this, "注册失败");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        progressDialog.dismiss();
                        ToastUtil.toast(RegisterActivity.this, "网络错误");
                    }
                });
    }
}
