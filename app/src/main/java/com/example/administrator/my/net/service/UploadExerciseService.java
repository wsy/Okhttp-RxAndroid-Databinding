package com.example.administrator.my.net.service;

import com.example.administrator.my.exercise.ExerciseDay;

import java.util.List;

import rx.Observable;

/**
 * Created by wsy on 5/7/16.
 */
public interface UploadExerciseService {
    Observable upload(List<ExerciseDay> exerciseDays);
}
