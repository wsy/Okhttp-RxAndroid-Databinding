package utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.administrator.my.application;

/**
 * Created by Administrator on 2016/3/25.
 */
public class Preference {

    /**
     * 保存在手机里面的文件名
     */
    private static final String FILE_NAME = "share_data";

    public static String getStr(String key) {
        SharedPreferences sp = application.getContext().getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        return sp.getString(key, "");
    }

    public static String getStr(Context context, String key, String defaultStr) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        return sp.getString(key, defaultStr);
    }

    public static void putStr(Context context, String key, String value) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static int getInt(Context context, String key, int defaultStr) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        return sp.getInt(key, defaultStr);
    }

    public static void putInt(Context context, String key, int value) {
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(key, value);
        editor.commit();
    }

}
