package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by wsy on 4/26/16.
 */
public class DateUtil {
    /**
     * @param time  秒数,不是毫秒
     * @param year
     * @param month 从0开始计算
     * @param day
     * @return
     */
    public static boolean isThatDay(long time, int year, int month, int day) {
        String timeStr = DateUtil.getTimeStr(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(timeStr);
            Calendar calendar = Calendar.getInstance();
            calendar.clear();
            calendar.setTime(date);
            int testYear = calendar.get(Calendar.YEAR);
            int testMonth = calendar.get(Calendar.MONTH);
            int testDay = calendar.get(Calendar.DAY_OF_MONTH);
            return year == testYear && month == testMonth && day == testDay;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @param time 秒数,不是毫秒
     * @return
     */
    public static boolean isToday(long time) {
        Calendar calendar = Calendar.getInstance();
        int tYear = calendar.get(Calendar.YEAR);
        int tMonth = calendar.get(Calendar.MONTH);
        int tDay = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println(tYear + " " + tMonth + " " + tDay);
        String timeStr = DateUtil.getTimeStr(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(timeStr);
            calendar.clear();
            calendar.setTime(date);
            int testYear = calendar.get(Calendar.YEAR);
            int testMonth = calendar.get(Calendar.MONTH);
            int testDay = calendar.get(Calendar.DAY_OF_MONTH);
            System.out.println(testYear + " " + testMonth + " " + testDay);
            return tYear == testYear && tMonth == testMonth && tDay == testDay;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * @param time 秒数,不是毫秒
     * @return
     */
    public static String getTimeStr(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String dateStr = sdf.format(new Date(time * 1000));
        return dateStr;
    }

    public static Date getDate(String str) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getDateStr(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = sdf.format(date);
        return dateStr;
    }

    public static String getDateStr(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = sdf.format(new Date(time * 1000));
        return dateStr;
    }

    /**
     * @param timeGap 秒数,不是毫秒
     * @return
     */
    public static String gapToStr(long timeGap) {
        int totalMin = (int) timeGap / 60;
        int totalHour = (int) totalMin / 60;
        int second = (int) timeGap % 60;
        int min = (int) totalMin % 60;
        int hour = totalHour;
        StringBuffer gapStr = new StringBuffer();
        if (hour > 0) {
            gapStr.append(hour + "小时");
        }
        if (min > 0) {
            gapStr.append(min + "分钟");
        }
        if (second > 0) {
            gapStr.append(second + "秒");
        }
        return gapStr.toString();
    }
}
