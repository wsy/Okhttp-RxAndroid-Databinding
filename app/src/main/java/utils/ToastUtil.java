package utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by wsy on 4/21/16.
 */
public class ToastUtil {
    public static void toast(Context context, String info) {
        Toast.makeText(context, info, Toast.LENGTH_LONG).show();
    }
}
