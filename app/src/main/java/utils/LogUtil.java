package utils;

import android.content.Context;
import android.util.Log;

/**
 * Created by wsy on 4/21/16.
 */
public class LogUtil {
    public static void log(Class c, String info) {
        Log.i(c.getSimpleName(), info);
    }

    public static void log(String tag, String info) {
        if (tag != null && info != null)
            Log.i(tag, info);
    }
}
