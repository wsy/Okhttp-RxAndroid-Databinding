package utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import com.example.administrator.okhttp_rxandroid_databinding.R;

/**
 * Created by wsy on 4/28/16.
 */
public class SysUtil {
    public static void notify(Context context, String notifyStr) {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // API16之后才支持
        Notification notify3 = new Notification.Builder(context)
                .setSmallIcon(R.drawable.notification)
                .setTicker(notifyStr)
                .setContentTitle("提醒")
                .setContentText(notifyStr)
//                .setContentIntent(pendingIntent3)
                .setNumber(1)
                .build(); // 需要注意build()是在API
        // level16及之后增加的，API11可以使用getNotificatin()来替代
        notify3.flags |= Notification.FLAG_AUTO_CANCEL; // FLAG_AUTO_CANCEL表明当通知被用户点击时，通知将被清除。
        mNotificationManager.notify(1, notify3);// 步骤4：通过通知管理器来发起通知。如果id不同，则每click，在status哪里增加
    }
}
