package com.example.administrator.okhttp_rxandroid_databinding;

import android.app.Application;
import android.content.ContentValues;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.example.administrator.my.db.TableConfig;
import dbUtil.UseDatabase;
import com.example.administrator.my.db.model.InsertModel;
import com.example.administrator.my.db.model.Record;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }
    @SmallTest
    public void testInsert(){
        UseDatabase useDatabase=new UseDatabase(getContext());
        useDatabase.opendb();
        useDatabase.insert(new InsertModel() {
            @Override
            public ContentValues values() {
                Record record=new Record();
                record.getValues().put("address","test address");
                record.getValues().put("update",0);
                return record.getValues();
            }

            @Override
            public String table() {
                return TableConfig.RECORD;
            }
        });
    }

    @SmallTest
    public void testQuery(){

    }
}